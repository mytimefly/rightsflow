<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropertyRepository")
 * @ORM\HasLifecycleCallbacks
 * @ExclusionPolicy("all")
 */
class Property
{
    const COMPLEX_TYPE_RETIREMENT = 1;
    const COMPLEX_TYPE_RESORT_HOLIDAY = 2;
    const COMPLEX_TYPE_PERMANENT = 3;
    const COMPLEX_TYPE_OFF_THE_PLAN = 4;
    const COMPLEX_TYPE_CORPORATE = 5;
    const COMPLEX_TYPE_STUDENT_ACCOM = 6;

    const LISTING_TYPE_EXCL = 1;
    const LISTING_TYPE_OPEN = 2;
    const LISTING_TYPE_SOLE = 3;
    const LISTING_TYPE_JOINT = 4;

    const MODULE_ACCOM = 1;
    const MODULE_STANDARD = 2;
    const MODULE_OTHER = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_UNDER_CONTRACT = 2;
    const STATUS_SOLD = 3;
    const STATUS_NOT_ACTIVE = 4;
    const STATUS_PENDING = 5;

    public static $statuses = [
        Property::STATUS_PENDING => 'Pending',
        Property::STATUS_ACTIVE => 'For Sale',
        Property::STATUS_UNDER_CONTRACT => 'Under Contract',
        Property::STATUS_SOLD => 'Sold',
        Property::STATUS_NOT_ACTIVE => 'Cancelled'
    ];

    const OFFICE_TYPE_ON_TITLE = 1;
    const OFFICE_TYPE_EXCLUSIVE_USE = 2;

    public static $office_types = [
        self::OFFICE_TYPE_ON_TITLE => 'On Title',
        self::OFFICE_TYPE_EXCLUSIVE_USE => 'Exclusive Use'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Expose
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Expose
     */
    private $listingDate;

    /**
     * @ORM\Column(type="integer")
     * @Expose
     */
    private $listingType;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     * @Expose
     */
    private $listingExpiry;

    /**
     * @ORM\Column(type="integer")
     * @Expose
     */
    private $listingComplexType;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $primaryLocation;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $secondaryLocation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $alertAllBuyers;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $address;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $suburb;

    /**
     * @ORM\Column(type="string")
     * @Expose
     */
    private $state;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $planNumber;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $parish;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $county;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     * @Expose
     */
    private $lastSoldDate;

    /**
     * @ORM\Column(type="boolean")
     * @Expose
     */
    private $featured;


    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $receiveMarketingEmails;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $primaryContactName;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $secondaryContactName;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $managerUnitNumber;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $websiteUrl;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $uBDReference;


    // Letting

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     * @Expose
     */
    private $yearBuilt;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     * @Expose
     */
    private $totalNumUnits;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numPermanentUnits;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numHolidayUnits;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numCorporateUnit;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $totalNumLettingUnits;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numOutsideLettingLockUp;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numOutsideLettingOutsideAgents;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numOutsideLettingSelfManaged;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numOutsideLettingOwnerOccupied;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $totalNumOutsideLettingUnits;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $numMultipleUnitOwnersInComplex;


    // Office

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     * @Expose
     */
    private $managerUnitSize;

    /**
     * @ORM\Column(type="text", nullable=TRUE)
     * @Expose
     */
    private $managerUnitDescription;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $storage;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $officeType;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $officeReception;

    /**
     * @ORM\Column(type="text", nullable=TRUE)
     * @Expose
     */
    private $officeHours;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $officeSoftware;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     * @Expose
     */
    private $module;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $agreementTerm;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
    private $yearsToRun;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $bodyCorpSecretary;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $bodyCorpManager;

    /**
     * @ORM\Column(type="boolean", nullable=FALSE)
     */
    private $lettingAgreementAvailable;

    /**
     * @ORM\Column(type="boolean", nullable=FALSE)
     */
    private $caretakingAgreementAvailable;


    // Tarrifs

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $permanentTariff;


    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $corporateTariff;


    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $holidayTariff;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
	private $bodycorporateSalary;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $salaryAnniversary;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $bodycorporateSalaryReview;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
	private $managerUnitBodyCorporateFees;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
	private $managerUnitRates;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
	private $sinkingFundBalance;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
	private $managerUnitPrice;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     */
	private $managementRightsPrice;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     * @Expose
     */
	private $totalPrice;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $priceDisplayAlt;

    /**
     * @ORM\Column(type="integer", nullable=TRUE)
     * @Expose
     */
	private $netProfit;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $registeredProprietorManagersUnit;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $registeredProprietorRightsBusiness;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $sellerSolicitor;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
	private $sellerAccountant;


    // Publishing

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     * @Expose
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=TRUE)
     * @Expose
     */
    private $content;

    /**
     * @ORM\Column(type="text", nullable=TRUE)
     */
    private $contentHidden;

    /**
     * @ORM\ManyToOne(targetEntity="Agent", inversedBy="properties")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     * @Type("AppBundle\Entity\Agent")
     * @Expose
     */
    protected $agent;

    /**
     * @ORM\OneToOne(targetEntity="Deal")
     * @ORM\JoinColumn(name="deal_id", referencedColumnName="id")
     */
    private $deal;

    /**
     * @ORM\Column(type="boolean", nullable=FALSE)
     */
    protected $archived;

    /**
     * @var File
     *
     * @ORM\OneToMany(targetEntity="PropertyFile", mappedBy="property", cascade={"persist"})
     * @Expose
     * @MaxDepth(1)
     */
    private $files;

    /**
     * @var ArrayCollection
     */
    private $uploadedFiles;

    /**
     * @ORM\ManyToMany(targetEntity="PropertyFeature", inversedBy="properties", cascade={"persist", "remove"}, orphanRemoval=TRUE)
     * @ORM\JoinTable(name="property_features_xref")
     * @Expose
     * @MaxDepth(1)
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity="PropertyNote", mappedBy="property", cascade={"persist", "remove"}, orphanRemoval=TRUE)
     * @ORM\OrderBy({"modifiedAt" = "DESC"})
     */
    private $notes;

    /**
     * @ORM\Column(type="integer")
     * @Expose
     */
    private $status = self::STATUS_PENDING;

    /**
     * @ORM\Column(type="boolean", nullable=FALSE)
     */
    private $showOnWebsite = true;

    /**
     * @ORM\Column(type="boolean", nullable=FALSE)
     */
    private $showOnExternalWebsites = true;

    /**
     * @ORM\Column(type="boolean", nullable=FALSE)
     */
    private $showInEmailBlasts = true;

    /**
     * @ORM\Column(type="datetime", nullable=TRUE)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=TRUE)
     */
    protected $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listingDate
     *
     * @param \DateTime $listingDate
     *
     * @return Property
     */
    public function setListingDate($listingDate)
    {
        $this->listingDate = $listingDate;

        return $this;
    }

    /**
     * Get listingDate
     *
     * @return \DateTime
     */
    public function getListingDate()
    {
        return $this->listingDate;
    }

    /**
     * Set listingType
     *
     * @param integer $listingType
     *
     * @return Property
     */
    public function setListingType($listingType)
    {
        $this->listingType = $listingType;

        return $this;
    }

    /**
     * Get listingType
     *
     * @return integer
     */
    public function getListingType()
    {
        return $this->listingType;
    }

    /**
     * Set listingExpiry
     *
     * @param \DateTime $listingExpiry
     *
     * @return Property
     */
    public function setListingExpiry($listingExpiry)
    {
        $this->listingExpiry = $listingExpiry;

        return $this;
    }

    /**
     * Get listingExpiry
     *
     * @return \DateTime
     */
    public function getListingExpiry()
    {
        return $this->listingExpiry;
    }

    /**
     * Set listingComplexType
     *
     * @param integer $listingComplexType
     *
     * @return Property
     */
    public function setListingComplexType($listingComplexType)
    {
        $this->listingComplexType = $listingComplexType;

        return $this;
    }

    /**
     * Get listingComplexType
     *
     * @return integer
     */
    public function getListingComplexType()
    {
        return $this->listingComplexType;
    }

    /**
     * Set primaryLocation
     *
     * @param string $primaryLocation
     *
     * @return Property
     */
    public function setPrimaryLocation($primaryLocation)
    {
        $this->primaryLocation = $primaryLocation;

        return $this;
    }

    /**
     * Get primaryLocation
     *
     * @return string
     */
    public function getPrimaryLocation()
    {
        return $this->primaryLocation;
    }

    /**
     * Set secondaryLocation
     *
     * @param string $secondaryLocation
     *
     * @return Property
     */
    public function setSecondaryLocation($secondaryLocation)
    {
        $this->secondaryLocation = $secondaryLocation;

        return $this;
    }

    /**
     * Get secondaryLocation
     *
     * @return string
     */
    public function getSecondaryLocation()
    {
        return $this->secondaryLocation;
    }

    /**
     * Set alertAllBuyers
     *
     * @param boolean $alertAllBuyers
     *
     * @return Property
     */
    public function setAlertAllBuyers($alertAllBuyers)
    {
        $this->alertAllBuyers = $alertAllBuyers;

        return $this;
    }

    /**
     * Get alertAllBuyers
     *
     * @return boolean
     */
    public function getAlertAllBuyers()
    {
        return $this->alertAllBuyers;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Property
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     *
     * @return Property
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Property
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Property
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set planNumber
     *
     * @param string $planNumber
     *
     * @return Property
     */
    public function setPlanNumber($planNumber)
    {
        $this->planNumber = $planNumber;

        return $this;
    }

    /**
     * Get planNumber
     *
     * @return string
     */
    public function getPlanNumber()
    {
        return $this->planNumber;
    }

    /**
     * Set parish
     *
     * @param string $parish
     *
     * @return Property
     */
    public function setParish($parish)
    {
        $this->parish = $parish;

        return $this;
    }

    /**
     * Get parish
     *
     * @return string
     */
    public function getParish()
    {
        return $this->parish;
    }

    /**
     * Set county
     *
     * @param string $county
     *
     * @return Property
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set lastSoldDate
     *
     * @param \DateTime $lastSoldDate
     *
     * @return Property
     */
    public function setLastSoldDate($lastSoldDate)
    {
        $this->lastSoldDate = $lastSoldDate;

        return $this;
    }

    /**
     * Get lastSoldDate
     *
     * @return \DateTime
     */
    public function getLastSoldDate()
    {
        return $this->lastSoldDate;
    }

    /**
     * Set featured
     *
     * @param boolean $featured
     *
     * @return Property
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return boolean
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Property
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return Property
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Property
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Property
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set receiveMarketingEmails
     *
     * @param boolean $receiveMarketingEmails
     *
     * @return Property
     */
    public function setreceiveMarketingEmails($receiveMarketingEmails)
    {
        $this->receiveMarketingEmails = $receiveMarketingEmails;

        return $this;
    }

    /**
     * Get receiveMarketingEmails
     *
     * @return boolean
     */
    public function getreceiveMarketingEmails()
    {
        return $this->receiveMarketingEmails;
    }

    /**
     * Set primaryContactName
     *
     * @param string $primaryContactName
     *
     * @return Property
     */
    public function setPrimaryContactName($primaryContactName)
    {
        $this->primaryContactName = $primaryContactName;

        return $this;
    }

    /**
     * Get primaryContactName
     *
     * @return string
     */
    public function getPrimaryContactName()
    {
        return $this->primaryContactName;
    }

    /**
     * Set secondaryContactName
     *
     * @param string $secondaryContactName
     *
     * @return Property
     */
    public function setSecondaryContactName($secondaryContactName)
    {
        $this->secondaryContactName = $secondaryContactName;

        return $this;
    }

    /**
     * Get secondaryContactName
     *
     * @return string
     */
    public function getSecondaryContactName()
    {
        return $this->secondaryContactName;
    }

    /**
     * Set managerUnitNumber
     *
     * @param string $managerUnitNumber
     *
     * @return Property
     */
    public function setManagerUnitNumber($managerUnitNumber)
    {
        $this->managerUnitNumber = $managerUnitNumber;

        return $this;
    }

    /**
     * Get managerUnitNumber
     *
     * @return string
     */
    public function getManagerUnitNumber()
    {
        return $this->managerUnitNumber;
    }

    /**
     * Set websiteUrl
     *
     * @param string $websiteUrl
     *
     * @return Property
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get websiteUrl
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * Set uBDReference
     *
     * @param string $uBDReference
     *
     * @return Property
     */
    public function setUBDReference($uBDReference)
    {
        $this->uBDReference = $uBDReference;

        return $this;
    }

    /**
     * Get uBDReference
     *
     * @return string
     */
    public function getUBDReference()
    {
        return $this->uBDReference;
    }

    /**
     * Set yearBuilt
     *
     * @param \DateTime $yearBuilt
     *
     * @return Property
     */
    public function setYearBuilt($yearBuilt)
    {
        $this->yearBuilt = $yearBuilt;

        return $this;
    }

    /**
     * Get yearBuilt
     *
     * @return \DateTime
     */
    public function getYearBuilt()
    {
        return $this->yearBuilt;
    }

    /**
     * Set totalNumUnits
     *
     * @param integer $totalNumUnits
     *
     * @return Property
     */
    public function setTotalNumUnits($totalNumUnits)
    {
        $this->totalNumUnits = $totalNumUnits;

        return $this;
    }

    /**
     * Get totalNumUnits
     *
     * @return integer
     */
    public function getTotalNumUnits()
    {
        return ($this->totalNumUnits) ? $this->totalNumUnits : 0;
    }

    /**
     * Set numPermanentUnits
     *
     * @param integer $numPermanentUnits
     *
     * @return Property
     */
    public function setNumPermanentUnits($numPermanentUnits)
    {
        $this->numPermanentUnits = $numPermanentUnits;

        return $this;
    }

    /**
     * Get numPermanentUnits
     *
     * @return integer
     */
    public function getNumPermanentUnits()
    {
        return ($this->numPermanentUnits) ? $this->numPermanentUnits : 0;
    }

    /**
     * Set numHolidayUnits
     *
     * @param integer $numHolidayUnits
     *
     * @return Property
     */
    public function setNumHolidayUnits($numHolidayUnits)
    {
        $this->numHolidayUnits = $numHolidayUnits;

        return $this;
    }

    /**
     * Get numHolidayUnits
     *
     * @return integer
     */
    public function getNumHolidayUnits()
    {
        return ($this->numHolidayUnits) ? $this->numHolidayUnits : 0;
    }

    /**
     * Set numCorporateUnit
     *
     * @param integer $numCorporateUnit
     *
     * @return Property
     */
    public function setNumCorporateUnit($numCorporateUnit)
    {
        $this->numCorporateUnit = $numCorporateUnit;

        return $this;
    }

    /**
     * Get numCorporateUnit
     *
     * @return integer
     */
    public function getNumCorporateUnit()
    {
        return ($this->numCorporateUnit) ? $this->numCorporateUnit : 0;
    }

    /**
     * Set numOutsideLettingLockUp
     *
     * @param integer $numOutsideLettingLockUp
     *
     * @return Property
     */
    public function setNumOutsideLettingLockUp($numOutsideLettingLockUp)
    {
        $this->numOutsideLettingLockUp = $numOutsideLettingLockUp;

        return $this;
    }

    /**
     * Get numOutsideLettingLockUp
     *
     * @return integer
     */
    public function getNumOutsideLettingLockUp()
    {
        return ($this->numOutsideLettingLockUp) ? $this->numOutsideLettingLockUp : 0;
    }

    /**
     * Set numOutsideLettingOutsideAgents
     *
     * @param integer $numOutsideLettingOutsideAgents
     *
     * @return Property
     */
    public function setNumOutsideLettingOutsideAgents($numOutsideLettingOutsideAgents)
    {
        $this->numOutsideLettingOutsideAgents = $numOutsideLettingOutsideAgents;

        return $this;
    }

    /**
     * Get numOutsideLettingOutsideAgents
     *
     * @return integer
     */
    public function getNumOutsideLettingOutsideAgents()
    {
        return ($this->numOutsideLettingOutsideAgents) ? $this->numOutsideLettingOutsideAgents : 0;
    }

    /**
     * Set numOutsideLettingSelfManaged
     *
     * @param integer $numOutsideLettingSelfManaged
     *
     * @return Property
     */
    public function setNumOutsideLettingSelfManaged($numOutsideLettingSelfManaged)
    {
        $this->numOutsideLettingSelfManaged = $numOutsideLettingSelfManaged;

        return $this;
    }

    /**
     * Get numOutsideLettingSelfManaged
     *
     * @return integer
     */
    public function getNumOutsideLettingSelfManaged()
    {
        return ($this->numOutsideLettingSelfManaged) ? $this->numOutsideLettingSelfManaged : 0;
    }

    /**
     * Set numOutsideLettingOwnerOccupied
     *
     * @param integer $numOutsideLettingOwnerOccupied
     *
     * @return Property
     */
    public function setNumOutsideLettingOwnerOccupied($numOutsideLettingOwnerOccupied)
    {
        $this->numOutsideLettingOwnerOccupied = $numOutsideLettingOwnerOccupied;

        return $this;
    }

    /**
     * Get numOutsideLettingOwnerOccupied
     *
     * @return integer
     */
    public function getNumOutsideLettingOwnerOccupied()
    {
        return ($this->numOutsideLettingOwnerOccupied) ? $this->numOutsideLettingOwnerOccupied : 0;
    }

    /**
     * Set numMultipleUnitOwnersInComplex
     *
     * @param integer $numMultipleUnitOwnersInComplex
     *
     * @return Property
     */
    public function setNumMultipleUnitOwnersInComplex($numMultipleUnitOwnersInComplex)
    {
        $this->numMultipleUnitOwnersInComplex = $numMultipleUnitOwnersInComplex;

        return $this;
    }

    /**
     * Get numMultipleUnitOwnersInComplex
     *
     * @return integer
     */
    public function getNumMultipleUnitOwnersInComplex()
    {
        return ($this->numMultipleUnitOwnersInComplex) ? $this->numMultipleUnitOwnersInComplex : 0;
    }

    /**
     * Set managerUnitSize
     *
     * @param integer $managerUnitSize
     *
     * @return Property
     */
    public function setManagerUnitSize($managerUnitSize)
    {
        $this->managerUnitSize = $managerUnitSize;

        return $this;
    }

    /**
     * Get managerUnitSize
     *
     * @return integer
     */
    public function getManagerUnitSize()
    {
        return $this->managerUnitSize;
    }

    /**
     * Set managerUnitDescription
     *
     * @param text $managerUnitDescription
     *
     * @return Property
     */
    public function setManagerUnitDescription($managerUnitDescription)
    {
        $this->managerUnitDescription = $managerUnitDescription;

        return $this;
    }

    /**
     * Get managerUnitDescription
     *
     * @return string
     */
    public function getManagerUnitDescription()
    {
        return $this->managerUnitDescription;
    }

    /**
     * Set storage
     *
     * @param string $storage
     *
     * @return Property
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * Get storage
     *
     * @return string
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Set officeReception
     *
     * @param string $officeReception
     *
     * @return Property
     */
    public function setOfficeReception($officeReception)
    {
        $this->officeReception = $officeReception;

        return $this;
    }

    /**
     * Get officeReception
     *
     * @return string
     */
    public function getOfficeReception()
    {
        return $this->officeReception;
    }

    /**
     * Set officeHours
     *
     * @param string $officeHours
     *
     * @return Property
     */
    public function setOfficeHours($officeHours)
    {
        $this->officeHours = $officeHours;

        return $this;
    }

    /**
     * Get officeHours
     *
     * @return string
     */
    public function getOfficeHours()
    {
        return $this->officeHours;
    }

    /**
     * Set officeSoftware
     *
     * @param string $officeSoftware
     *
     * @return Property
     */
    public function setOfficeSoftware($officeSoftware)
    {
        $this->officeSoftware = $officeSoftware;

        return $this;
    }

    /**
     * Get officeSoftware
     *
     * @return string
     */
    public function getOfficeSoftware()
    {
        return $this->officeSoftware;
    }

    /**
     * Set module
     *
     * @param integer $module
     *
     * @return Property
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return integer
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set agreementTerm
     *
     * @param integer $agreementTerm
     *
     * @return Property
     */
    public function setAgreementTerm($agreementTerm)
    {
        $this->agreementTerm = $agreementTerm;

        return $this;
    }

    /**
     * Get agreementTerm
     *
     * @return integer
     */
    public function getAgreementTerm()
    {
        return $this->agreementTerm;
    }

    /**
     * Set yearsToRun
     *
     * @param integer $yearsToRun
     *
     * @return Property
     */
    public function setYearsToRun($yearsToRun)
    {
        $this->yearsToRun = $yearsToRun;

        return $this;
    }

    /**
     * Get yearsToRun
     *
     * @return integer
     */
    public function getYearsToRun()
    {
        return $this->yearsToRun;
    }

    /**
     * Set bodyCorpSecretary
     *
     * @param string $bodyCorpSecretary
     *
     * @return Property
     */
    public function setBodyCorpSecretary($bodyCorpSecretary)
    {
        $this->bodyCorpSecretary = $bodyCorpSecretary;

        return $this;
    }

    /**
     * Get bodyCorpSecretary
     *
     * @return string
     */
    public function getBodyCorpSecretary()
    {
        return $this->bodyCorpSecretary;
    }

    /**
     * Set bodyCorpManager
     *
     * @param string $bodyCorpManager
     *
     * @return Property
     */
    public function setBodyCorpManager($bodyCorpManager)
    {
        $this->bodyCorpManager = $bodyCorpManager;

        return $this;
    }

    /**
     * Get bodyCorpManager
     *
     * @return string
     */
    public function getBodyCorpManager()
    {
        return $this->bodyCorpManager;
    }

    /**
     * Set lettingAgreementAvailable
     *
     * @param boolean $lettingAgreementAvailable
     *
     * @return Property
     */
    public function setLettingAgreementAvailable($lettingAgreementAvailable)
    {
        $this->lettingAgreementAvailable = $lettingAgreementAvailable;

        return $this;
    }

    /**
     * Get lettingAgreementAvailable
     *
     * @return boolean
     */
    public function getLettingAgreementAvailable()
    {
        return $this->lettingAgreementAvailable;
    }

    /**
     * Set caretakingAgreementAvailable
     *
     * @param boolean $caretakingAgreementAvailable
     *
     * @return Property
     */
    public function setCaretakingAgreementAvailable($caretakingAgreementAvailable)
    {
        $this->caretakingAgreementAvailable = $caretakingAgreementAvailable;

        return $this;
    }

    /**
     * Get caretakingAgreementAvailable
     *
     * @return boolean
     */
    public function getCaretakingAgreementAvailable()
    {
        return $this->caretakingAgreementAvailable;
    }

    /**
     * Set totalNumLettingUnits
     *
     * @param integer $totalNumLettingUnits
     *
     * @return Property
     */
    public function setTotalNumLettingUnits($totalNumLettingUnits)
    {
        $this->totalNumLettingUnits = $totalNumLettingUnits;

        return $this;
    }

    /**
     * Get totalNumLettingUnits
     *
     * @return integer
     */
    public function getTotalNumLettingUnits()
    {
        return ($this->totalNumLettingUnits) ? $this->totalNumLettingUnits : 0;
    }

    /**
     * Set totalNumOutsideLettingUnits
     *
     * @param integer $totalNumOutsideLettingUnits
     *
     * @return Property
     */
    public function setTotalNumOutsideLettingUnits($totalNumOutsideLettingUnits)
    {
        $this->totalNumOutsideLettingUnits = $totalNumOutsideLettingUnits;

        return $this;
    }

    /**
     * Get totalNumOutsideLettingUnits
     *
     * @return integer
     */
    public function getTotalNumOutsideLettingUnits()
    {
        return ($this->totalNumOutsideLettingUnits) ? $this->totalNumOutsideLettingUnits : 0;
    }

    /**
     * Set permanentTariff
     *
     * @param string $permanentTariff
     *
     * @return Property
     */
    public function setPermanentTariff($permanentTariff)
    {
        $this->permanentTariff = $permanentTariff;

        return $this;
    }

    /**
     * Get permanentTariff
     *
     * @return string
     */
    public function getPermanentTariff()
    {
        return $this->permanentTariff;
    }

    /**
     * Set corporateTariff
     *
     * @param string $corporateTariff
     *
     * @return Property
     */
    public function setCorporateTariff($corporateTariff)
    {
        $this->corporateTariff = $corporateTariff;

        return $this;
    }

    /**
     * Get corporateTariff
     *
     * @return string
     */
    public function getCorporateTariff()
    {
        return $this->corporateTariff;
    }

    /**
     * Set holidayTariff
     *
     * @param string $holidayTariff
     *
     * @return Property
     */
    public function setHolidayTariff($holidayTariff)
    {
        $this->holidayTariff = $holidayTariff;

        return $this;
    }

    /**
     * Get holidayTariff
     *
     * @return string
     */
    public function getHolidayTariff()
    {
        return $this->holidayTariff;
    }

    /**
     * Set bodycorporateSalary
     *
     * @param integer $bodycorporateSalary
     *
     * @return Property
     */
    public function setBodycorporateSalary($bodycorporateSalary)
    {
        $this->bodycorporateSalary = $bodycorporateSalary;

        return $this;
    }

    /**
     * Get bodycorporateSalary
     *
     * @return integer
     */
    public function getBodycorporateSalary()
    {
        return $this->bodycorporateSalary;
    }

    /**
     * Set salaryAnniversary
     *
     * @param string $salaryAnniversary
     *
     * @return Property
     */
    public function setSalaryAnniversary($salaryAnniversary)
    {
        $this->salaryAnniversary = $salaryAnniversary;

        return $this;
    }

    /**
     * Get salaryAnniversary
     *
     * @return string
     */
    public function getSalaryAnniversary()
    {
        return $this->salaryAnniversary;
    }

    /**
     * Set bodycorporateSalaryReview
     *
     * @param string $bodycorporateSalaryReview
     *
     * @return Property
     */
    public function setBodycorporateSalaryReview($bodycorporateSalaryReview)
    {
        $this->bodycorporateSalaryReview = $bodycorporateSalaryReview;

        return $this;
    }

    /**
     * Get bodycorporateSalaryReview
     *
     * @return string
     */
    public function getBodycorporateSalaryReview()
    {
        return $this->bodycorporateSalaryReview;
    }

    /**
     * Set managerUnitBodyCorporateFees
     *
     * @param integer $managerUnitBodyCorporateFees
     *
     * @return Property
     */
    public function setManagerUnitBodyCorporateFees($managerUnitBodyCorporateFees)
    {
        $this->managerUnitBodyCorporateFees = $managerUnitBodyCorporateFees;

        return $this;
    }

    /**
     * Get managerUnitBodyCorporateFees
     *
     * @return integer
     */
    public function getManagerUnitBodyCorporateFees()
    {
        return $this->managerUnitBodyCorporateFees;
    }

    /**
     * Set managerUnitRates
     *
     * @param integer $managerUnitRates
     *
     * @return Property
     */
    public function setManagerUnitRates($managerUnitRates)
    {
        $this->managerUnitRates = $managerUnitRates;

        return $this;
    }

    /**
     * Get managerUnitRates
     *
     * @return integer
     */
    public function getManagerUnitRates()
    {
        return $this->managerUnitRates;
    }

    /**
     * Set sinkingFundBalance
     *
     * @param integer $sinkingFundBalance
     *
     * @return Property
     */
    public function setSinkingFundBalance($sinkingFundBalance)
    {
        $this->sinkingFundBalance = $sinkingFundBalance;

        return $this;
    }

    /**
     * Get sinkingFundBalance
     *
     * @return integer
     */
    public function getSinkingFundBalance()
    {
        return $this->sinkingFundBalance;
    }

    /**
     * Set managerUnitPrice
     *
     * @param integer $managerUnitPrice
     *
     * @return Property
     */
    public function setManagerUnitPrice($managerUnitPrice)
    {
        $this->managerUnitPrice = $managerUnitPrice;

        return $this;
    }

    /**
     * Get managerUnitPrice
     *
     * @return integer
     */
    public function getManagerUnitPrice()
    {
        return $this->managerUnitPrice;
    }

    /**
     * Set managementRightsPrice
     *
     * @param integer $managementRightsPrice
     *
     * @return Property
     */
    public function setManagementRightsPrice($managementRightsPrice)
    {
        $this->managementRightsPrice = $managementRightsPrice;

        return $this;
    }

    /**
     * Get managementRightsPrice
     *
     * @return integer
     */
    public function getManagementRightsPrice()
    {
        return $this->managementRightsPrice;
    }

    /**
     * Set totalPrice
     *
     * @param integer $totalPrice
     *
     * @return Property
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return integer
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set priceDisplayAlt
     *
     * @param string $priceDisplayAlt
     *
     * @return Property
     */
    public function setPriceDisplayAlt($priceDisplayAlt)
    {
        $this->priceDisplayAlt = $priceDisplayAlt;

        return $this;
    }

    /**
     * Get priceDisplayAlt
     *
     * @return string
     */
    public function getPriceDisplayAlt()
    {
        return $this->priceDisplayAlt;
    }

    /**
     * Set netProfit
     *
     * @param integer $netProfit
     *
     * @return Property
     */
    public function setNetProfit($netProfit)
    {
        $this->netProfit = $netProfit;

        return $this;
    }

    /**
     * Get netProfit
     *
     * @return integer
     */
    public function getNetProfit()
    {
        return $this->netProfit;
    }

    /**
     * Set registeredProprietorManagersUnit
     *
     * @param string $registeredProprietorManagersUnit
     *
     * @return Property
     */
    public function setRegisteredProprietorManagersUnit($registeredProprietorManagersUnit)
    {
        $this->registeredProprietorManagersUnit = $registeredProprietorManagersUnit;

        return $this;
    }

    /**
     * Get registeredProprietorManagersUnit
     *
     * @return string
     */
    public function getRegisteredProprietorManagersUnit()
    {
        return $this->registeredProprietorManagersUnit;
    }

    /**
     * Set registeredProprietorRightsBusiness
     *
     * @param string $registeredProprietorRightsBusiness
     *
     * @return Property
     */
    public function setRegisteredProprietorRightsBusiness($registeredProprietorRightsBusiness)
    {
        $this->registeredProprietorRightsBusiness = $registeredProprietorRightsBusiness;

        return $this;
    }

    /**
     * Get registeredProprietorRightsBusiness
     *
     * @return string
     */
    public function getRegisteredProprietorRightsBusiness()
    {
        return $this->registeredProprietorRightsBusiness;
    }

    /**
     * Set sellerSolicitor
     *
     * @param string $sellerSolicitor
     *
     * @return Property
     */
    public function setSellerSolicitor($sellerSolicitor)
    {
        $this->sellerSolicitor = $sellerSolicitor;

        return $this;
    }

    /**
     * Get sellerSolicitor
     *
     * @return string
     */
    public function getSellerSolicitor()
    {
        return $this->sellerSolicitor;
    }

    /**
     * Set sellerAccountant
     *
     * @param string $sellerAccountant
     *
     * @return Property
     */
    public function setSellerAccountant($sellerAccountant)
    {
        $this->sellerAccountant = $sellerAccountant;

        return $this;
    }

    /**
     * Get sellerAccountant
     *
     * @return string
     */
    public function getSellerAccountant()
    {
        return $this->sellerAccountant;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Property
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        if (!empty($this->title))
            return $this->title;
        else
            return '#'.$this->getId().' - '.$this->getSuburb();
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Property
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set contentHidden
     *
     * @param string $contentHidden
     *
     * @return Property
     */
    public function setContentHidden($contentHidden)
    {
        $this->contentHidden = $contentHidden;

        return $this;
    }

    /**
     * Get contentHidden
     *
     * @return string
     */
    public function getContentHidden()
    {
        return $this->contentHidden;
    }

    /**
     * Set agent
     *
     * @param \AppBundle\Entity\Agent $agent
     *
     * @return Property
     */
    public function setAgent(\AppBundle\Entity\Agent $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \AppBundle\Entity\Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return Property
     */
    public function setDeal(\AppBundle\Entity\Deal $deal = null)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return \AppBundle\Entity\Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return Property
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploadedFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add file
     *
     * @param \AppBundle\Entity\PropertyFile $file
     *
     * @return Property
     */
    public function addFile(\AppBundle\Entity\PropertyFile $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \AppBundle\Entity\PropertyFile $file
     */
    public function removeFile(\AppBundle\Entity\PropertyFile $file)
    {
        $file->deleteFile();
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return ArrayCollection
     */
    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    /**
     * @param ArrayCollection $uploadedFiles
     */
    public function setUploadedFiles($uploadedFiles)
    {
        $this->uploadedFiles = $uploadedFiles;
    }

    /**
     * @ORM\PreFlush()
     */
    public function upload()
    {
        if ($this->uploadedFiles) foreach($this->uploadedFiles as $uploadedFile)
        {
            if ($uploadedFile != NULL) {
                $file = new PropertyFile();
                /*
                 * These lines could be moved to the File Class constructor to factorize
                 * the File initialization and thus allow other classes to own Files
                 */
                $path = sha1(uniqid(mt_rand(), true)).'.'.$uploadedFile->guessExtension();
                $file->setPath($path);
                $file->setSize($uploadedFile->getClientSize());
                $file->setName($uploadedFile->getClientOriginalName());

                $uploadDir = $this->getUploadRootDir();
                $uploadedFile->move($uploadDir, $path);

                $this->getFiles()->add($file);
                $file->setProperty($this);

                unset($uploadedFile);
            }
        }
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/property';
    }

    /**
     * Add feature
     *
     * @param \AppBundle\Entity\PropertyFeature $feature
     *
     * @return Property
     */
    public function addFeature(\AppBundle\Entity\PropertyFeature $feature)
    {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param \AppBundle\Entity\PropertyFeature $feature
     */
    public function removeFeature(\AppBundle\Entity\PropertyFeature $feature)
    {
        $this->features->removeElement($feature);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\PropertyNote $note
     *
     * @return Property
     */
    public function addNote(\AppBundle\Entity\PropertyNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\PropertyNote $note
     */
    public function removeNote(\AppBundle\Entity\PropertyNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Property
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the status as a string label
     */
    public function getStatusString() {
        if ($status = self::$statuses[$this->status]) {
            return $status;
        }
        return 'Unknown';
    }

    /**
     * Set showOnWebsite
     *
     * @param boolean $showOnWebsite
     *
     * @return Property
     */
    public function setShowOnWebsite($showOnWebsite)
    {
        $this->showOnWebsite = $showOnWebsite;

        return $this;
    }

    /**
     * Get showOnWebsite
     *
     * @return boolean
     */
    public function getShowOnWebsite()
    {
        return $this->showOnWebsite;
    }

    /**
     * Set showOnExternalWebsites
     *
     * @param boolean $showOnExternalWebsites
     *
     * @return Property
     */
    public function setShowOnExternalWebsites($showOnExternalWebsites)
    {
        $this->showOnExternalWebsites = $showOnExternalWebsites;

        return $this;
    }

    /**
     * Get showOnExternalWebsites
     *
     * @return boolean
     */
    public function getShowOnExternalWebsites()
    {
        return $this->showOnExternalWebsites;
    }

    /**
     * Set showInEmailBlasts
     *
     * @param boolean $showInEmailBlasts
     *
     * @return Property
     */
    public function setShowInEmailBlasts($showInEmailBlasts)
    {
        $this->showInEmailBlasts = $showInEmailBlasts;

        return $this;
    }

    /**
     * Get showInEmailBlasts
     *
     * @return boolean
     */
    public function getShowInEmailBlasts()
    {
        return $this->showInEmailBlasts;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Agent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Agent
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Set officeType
     *
     * @param string $officeType
     *
     * @return Property
     */
    public function setOfficeType($officeType)
    {
        $this->officeType = $officeType;

        return $this;
    }

    /**
     * Get officeType
     *
     * @return string
     */
    public function getOfficeType()
    {
        return $this->officeType;
    }

    /**
     * Get officeTypeLabel
     *
     * @return string
     */
    public function getOfficeTypeLabel() {
        if (!$this->getOfficeType()) return '';
        return self::$office_types[$this->getOfficeType()];
    }
}
