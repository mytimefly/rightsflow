<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StaffCommissionsRepository")
 */
class StaffCommission {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Agent", inversedBy="commissions")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    protected $staffMember;

    /**
     * @ORM\ManyToOne(targetEntity="Deal", inversedBy="staffCommissions")
     * @ORM\JoinColumn(name="deal_id", referencedColumnName="id")
     */
    protected $deal;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $percentage;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $commission;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $super;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $total;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $gst;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     *
     * @return StaffCommission
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set commission
     *
     * @param string $commission
     *
     * @return StaffCommission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set super
     *
     * @param string $super
     *
     * @return StaffCommission
     */
    public function setSuper($super)
    {
        $this->super = $super;

        return $this;
    }

    /**
     * Get super
     *
     * @return string
     */
    public function getSuper()
    {
        return $this->super;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return StaffCommission
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set gst
     *
     * @param string $gst
     *
     * @return StaffCommission
     */
    public function setGst($gst)
    {
        $this->gst = $gst;

        return $this;
    }

    /**
     * Get gst
     *
     * @return string
     */
    public function getGst()
    {
        return $this->gst;
    }

    /**
     * Set staffMember
     *
     * @param \AppBundle\Entity\Agent $staffMember
     *
     * @return StaffCommission
     */
    public function setStaffMember(\AppBundle\Entity\Agent $staffMember = null)
    {
        $this->staffMember = $staffMember;

        return $this;
    }

    /**
     * Get staffMember
     *
     * @return \AppBundle\Entity\Agent
     */
    public function getStaffMember()
    {
        return $this->staffMember;
    }

    /**
     * Set deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return StaffCommission
     */
    public function setDeal(\AppBundle\Entity\Deal $deal = null)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return \AppBundle\Entity\Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }
}
