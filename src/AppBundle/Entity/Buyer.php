<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BuyerRepository")
 */
class Buyer extends Person {

    /**
     * @ORM\Column(type="boolean")
     */
    protected $emailConsent = false;

    /**
     * @ORM\Column(type="string")
     */
    protected $purchaserSolicitor = '';

    /**
     * @ORM\Column(type="string")
     */
    protected $regProprietorUnit = '';

    /**
     * @ORM\Column(type="string")
     */
    protected $regProprietorRightsBusiness = '';

    /**
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="buyer")
     */
    protected $deals;

    /**
     * @ORM\OneToMany(targetEntity="BuyerNote", mappedBy="buyer", cascade={"persist", "remove"}, orphanRemoval=TRUE)
     * @ORM\OrderBy({"updatedAt" = "DESC"})
     */
    private $notes;

    /**
     * Set emailConsent
     *
     * @param boolean $emailConsent
     *
     * @return Property
     */
    public function setEmailConsent($emailConsent)
    {
        $this->emailConsent = $emailConsent;

        return $this;
    }

    /**
     * Get emailConsent
     *
     * @return boolean
     */
    public function getEmailConsent()
    {
        return $this->emailConsent;
    }

    /**
     * @return string
     */
    public function hasEmailConsent() {
        return ($this->getEmailConsent()) ? 'Yes' : 'No';
    }

    /**
     * Set regProprietorUnit
     *
     * @param string $regProprietorUnit
     *
     * @return Buyer
     */
    public function setRegProprietorUnit($regProprietorUnit)
    {
        $this->regProprietorUnit = $regProprietorUnit;

        return $this;
    }

    /**
     * Get regProprietorUnit
     *
     * @return string
     */
    public function getRegProprietorUnit()
    {
        return $this->regProprietorUnit;
    }

    /**
     * Set regProprietorRightsBusiness
     *
     * @param string $regProprietorRightsBusiness
     *
     * @return Buyer
     */
    public function setRegProprietorRightsBusiness($regProprietorRightsBusiness)
    {
        $this->regProprietorRightsBusiness = $regProprietorRightsBusiness;

        return $this;
    }

    /**
     * Get regProprietorRightsBusiness
     *
     * @return string
     */
    public function getRegProprietorRightsBusiness()
    {
        return $this->regProprietorRightsBusiness;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return Buyer
     */
    public function addDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * Remove deal
     *
     * @param \AppBundle\Entity\Deal $deal
     */
    public function removeDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * Get deals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * Set purchaserSolicitor
     *
     * @param string $purchaserSolicitor
     *
     * @return Buyer
     */
    public function setPurchaserSolicitor($purchaserSolicitor)
    {
        $this->purchaserSolicitor = $purchaserSolicitor;

        return $this;
    }

    /**
     * Get purchaserSolicitor
     *
     * @return string
     */
    public function getPurchaserSolicitor()
    {
        return $this->purchaserSolicitor;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Buyer
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\BuyerNote $note
     *
     * @return Buyer
     */
    public function addNote(\AppBundle\Entity\BuyerNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\BuyerNote $note
     */
    public function removeNote(\AppBundle\Entity\BuyerNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
