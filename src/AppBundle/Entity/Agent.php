<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AgentRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 * @ExclusionPolicy("all")
 */
class Agent extends Person
{
    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    private $licenseNumber;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    private $licenseExpiry;

    /**
     * @ORM\OneToMany(targetEntity="Property", mappedBy="agent")
     */
    protected $properties;

    /**
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="listingAgent")
     */
    protected $deals;

    /**
     * @ORM\OneToMany(targetEntity="StaffCommission", mappedBy="staffMember")
     */
    protected $commissions;

    public function __construct() {
        $this->properties = new ArrayCollection();
        $this->deals = new ArrayCollection();
        $this->commissions = new ArrayCollection();
    }

    /**
     * Set licenseNumber
     *
     * @param string $licenseNumber
     *
     * @return Agent
     */
    public function setLicenseNumber($licenseNumber)
    {
        $this->licenseNumber = $licenseNumber;

        return $this;
    }

    /**
     * Get licenseNumber
     *
     * @return string
     */
    public function getLicenseNumber()
    {
        return $this->licenseNumber;
    }

    /**
     * Set licenseExpiry
     *
     * @param \DateTime $licenseExpiry
     *
     * @return Agent
     */
    public function setLicenseExpiry($licenseExpiry)
    {
        $this->licenseExpiry = $licenseExpiry;

        return $this;
    }

    /**
     * Get licenseExpiry
     *
     * @return \DateTime
     */
    public function getLicenseExpiry()
    {
        return $this->licenseExpiry;
    }

    /**
     * Add property
     *
     * @param \AppBundle\Entity\Agent $property
     *
     * @return Agent
     */
    public function addProperty(\AppBundle\Entity\Agent $property)
    {
        $this->properties[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param \AppBundle\Entity\Agent $property
     */
    public function removeProperty(\AppBundle\Entity\Agent $property)
    {
        $this->properties->removeElement($property);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Add deal
     *
     * @param \AppBundle\Entity\Deal $deal
     *
     * @return Agent
     */
    public function addDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * Remove deal
     *
     * @param \AppBundle\Entity\Deal $deal
     */
    public function removeDeal(\AppBundle\Entity\Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * Get deals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * Add commission
     *
     * @param \AppBundle\Entity\StaffCommission $commission
     *
     * @return Agent
     */
    public function addCommission(\AppBundle\Entity\StaffCommission $commission)
    {
        $this->commissions[] = $commission;

        return $this;
    }

    /**
     * Remove commission
     *
     * @param \AppBundle\Entity\StaffCommission $commission
     */
    public function removeCommission(\AppBundle\Entity\StaffCommission $commission)
    {
        $this->commissions->removeElement($commission);
    }

    /**
     * Get commissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommissions()
    {
        return $this->commissions;
    }
}
