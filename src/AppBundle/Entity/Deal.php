<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DealRepository")
 */
class Deal
{
    const STATUS_UNDER_CONTRACT = 1;
    const STATUS_ACCOUNTING_DUE_DILIGENCE = 2;
    const STATUS_LEGAL_DUE_DILIGENCE = 3;
    const STATUS_REQUEST_FOR_EXTENSION = 4;
    const STATUS_UNCONDITIONAL = 5;
    const STATUS_SETTLED = 6;
    const STATUS_NOT_PROCEEDING = 7;

    const SALE_SOURCE_WEB_ENQUIRY = 1;
    const SALE_SOURCE_STAFF = 2;
    const SALE_SOURCE_REFERRAL = 3;
    const SALE_SOURCE_ONSITE_MANAGER = 4;
    const SALE_SOURCE_OTHER = 5;

    // array of statuses that do not lock the property
    public static $statuses_open = array(
        Deal::STATUS_NOT_PROCEEDING
    );

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Agent", inversedBy="deals")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    protected $listingAgent;

    /**
     * @ORM\OneToMany(targetEntity="StaffCommission", mappedBy="deal")
     */
    protected $staffCommissions;

    /**
     * @ORM\Column(type="integer", nullable=FALSE)
     */
    protected $status;

    /**
     * @ORM\OneToOne(targetEntity="Property")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     */
    protected $property;

    /**
     * @ORM\ManyToOne(targetEntity="Buyer")
     * @ORM\JoinColumn(name="buyer_id", referencedColumnName="id")
     */
    protected $buyer;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $offerAcceptedDate;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    protected $saleSource;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $form6 = false;


    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $saleDate;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $deposit;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $depositDueDate;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $balance;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $balanceDueDate;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    protected $financeBank;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $financeDueDate;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    protected $accountant;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $accountantDiligenceDueDate;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    protected $legalDiligenceSolicitor;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $legalDiligenceDueDate;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $bodyCorpMeetingDate;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $committeeAgmDate;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $committeeEgmDate;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $settlementDate;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $unitPrice;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $commission;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $managementRightsPrice;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $managementRightsCommission;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $otherUnitPurchasePrice;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $otherUnitCommission;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $totalCommissionExclGst;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $totalCommissionGst;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $totalCommission;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $netIncome;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $multiplier;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $officePercentage;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $officeCommission;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $officeSuper;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $officeTotal;

    /**
     * @ORM\Column(type="decimal", nullable=TRUE)
     */
    protected $officeGst;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    protected $vendorForwardingAddress;

    /**
     * @ORM\Column(type="string", nullable=TRUE)
     */
    protected $conjunctionAgentAddress;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $paidAndFinalisedDate;

    /**
     * @ORM\Column(type="text", nullable=TRUE)
     */
    protected $comments;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $confirmationEmailSentDate;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $contractsSignedDate;

    /**
     * @ORM\Column(type="date", nullable=TRUE)
     */
    protected $submittedToSolicitorsDate;

    public function __construct() {
        $this->staffCommissions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Deal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }


    public function getStatusString() {
        switch($this->getStatus()) {
            case Deal::STATUS_ACCOUNTING_DUE_DILIGENCE:
                return 'Accounting Due Diligence';

            case Deal::STATUS_NOT_PROCEEDING:
                return 'Not Preceeding';

            case Deal::STATUS_LEGAL_DUE_DILIGENCE:
                return 'Legal Due Diligence';

            case Deal::STATUS_REQUEST_FOR_EXTENSION:
                return 'Request For Extension';

            case Deal::STATUS_SETTLED:
                return 'Settled';

            case Deal::STATUS_UNCONDITIONAL:
                return 'Unconditional';

            case Deal::STATUS_UNDER_CONTRACT:
                return 'Under Contract';

            default:
                return 'N/A';
        }
    }

    /**
     * Set saleSource
     *
     * @param string $saleSource
     *
     * @return Deal
     */
    public function setSaleSource($saleSource)
    {
        $this->saleSource = $saleSource;

        return $this;
    }

    /**
     * Get saleSource
     *
     * @return string
     */
    public function getSaleSource()
    {
        return $this->saleSource;
    }

    /**
     * Set form6
     *
     * @param boolean $form6
     *
     * @return Deal
     */
    public function setform6($form6)
    {
        $this->form6 = $form6;

        return $this;
    }

    /**
     * Get form6
     *
     * @return boolean
     */
    public function getform6()
    {
        return $this->form6;
    }

    /**
     * Set saleDate
     *
     * @param \DateTime $saleDate
     *
     * @return Deal
     */
    public function setSaleDate($saleDate)
    {
        $this->saleDate = $saleDate;

        return $this;
    }

    /**
     * Get saleDate
     *
     * @return \DateTime
     */
    public function getSaleDate()
    {
        return $this->saleDate;
    }

    /**
     * Set deposit
     *
     * @param string $deposit
     *
     * @return Deal
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return string
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set depositDueDate
     *
     * @param \DateTime $depositDueDate
     *
     * @return Deal
     */
    public function setDepositDueDate($depositDueDate)
    {
        $this->depositDueDate = $depositDueDate;

        return $this;
    }

    /**
     * Get depositDueDate
     *
     * @return \DateTime
     */
    public function getDepositDueDate()
    {
        return $this->depositDueDate;
    }

    /**
     * Set balance
     *
     * @param string $balance
     *
     * @return Deal
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set balanceDueDate
     *
     * @param \DateTime $balanceDueDate
     *
     * @return Deal
     */
    public function setBalanceDueDate($balanceDueDate)
    {
        $this->balanceDueDate = $balanceDueDate;

        return $this;
    }

    /**
     * Get balanceDueDate
     *
     * @return \DateTime
     */
    public function getBalanceDueDate()
    {
        return $this->balanceDueDate;
    }

    /**
     * Set financeBank
     *
     * @param string $financeBank
     *
     * @return Deal
     */
    public function setFinanceBank($financeBank)
    {
        $this->financeBank = $financeBank;

        return $this;
    }

    /**
     * Get financeBank
     *
     * @return string
     */
    public function getFinanceBank()
    {
        return $this->financeBank;
    }

    /**
     * Set financeDueDate
     *
     * @param \DateTime $financeDueDate
     *
     * @return Deal
     */
    public function setFinanceDueDate($financeDueDate)
    {
        $this->financeDueDate = $financeDueDate;

        return $this;
    }

    /**
     * Get financeDueDate
     *
     * @return \DateTime
     */
    public function getFinanceDueDate()
    {
        return $this->financeDueDate;
    }

    /**
     * Set accountant
     *
     * @param string $accountant
     *
     * @return Deal
     */
    public function setAccountant($accountant)
    {
        $this->accountant = $accountant;

        return $this;
    }

    /**
     * Get accountant
     *
     * @return string
     */
    public function getAccountant()
    {
        return $this->accountant;
    }

    /**
     * Set accountantDiligenceDueDate
     *
     * @param \DateTime $accountantDiligenceDueDate
     *
     * @return Deal
     */
    public function setAccountantDiligenceDueDate($accountantDiligenceDueDate)
    {
        $this->accountantDiligenceDueDate = $accountantDiligenceDueDate;

        return $this;
    }

    /**
     * Get accountantDiligenceDueDate
     *
     * @return \DateTime
     */
    public function getAccountantDiligenceDueDate()
    {
        return $this->accountantDiligenceDueDate;
    }

    /**
     * Set legalDiligenceDueDate
     *
     * @param \DateTime $legalDiligenceDueDate
     *
     * @return Deal
     */
    public function setLegalDiligenceDueDate($legalDiligenceDueDate)
    {
        $this->legalDiligenceDueDate = $legalDiligenceDueDate;

        return $this;
    }

    /**
     * Get legalDiligenceDueDate
     *
     * @return \DateTime
     */
    public function getLegalDiligenceDueDate()
    {
        return $this->legalDiligenceDueDate;
    }

    /**
     * Set bodyCorpMeetingDate
     *
     * @param \DateTime $bodyCorpMeetingDate
     *
     * @return Deal
     */
    public function setBodyCorpMeetingDate($bodyCorpMeetingDate)
    {
        $this->bodyCorpMeetingDate = $bodyCorpMeetingDate;

        return $this;
    }

    /**
     * Get bodyCorpMeetingDate
     *
     * @return \DateTime
     */
    public function getBodyCorpMeetingDate()
    {
        return $this->bodyCorpMeetingDate;
    }

    /**
     * Set committeeAgmDate
     *
     * @param \DateTime $committeeAgmDate
     *
     * @return Deal
     */
    public function setCommitteeAgmDate($committeeAgmDate)
    {
        $this->committeeAgmDate = $committeeAgmDate;

        return $this;
    }

    /**
     * Get committeeAgmDate
     *
     * @return \DateTime
     */
    public function getCommitteeAgmDate()
    {
        return $this->committeeAgmDate;
    }

    /**
     * Set committeeEgmDate
     *
     * @param \DateTime $committeeEgmDate
     *
     * @return Deal
     */
    public function setCommitteeEgmDate($committeeEgmDate)
    {
        $this->committeeEgmDate = $committeeEgmDate;

        return $this;
    }

    /**
     * Get committeeEgmDate
     *
     * @return \DateTime
     */
    public function getCommitteeEgmDate()
    {
        return $this->committeeEgmDate;
    }

    /**
     * Set settlementDate
     *
     * @param \DateTime $settlementDate
     *
     * @return Deal
     */
    public function setSettlementDate($settlementDate)
    {
        $this->settlementDate = $settlementDate;

        return $this;
    }

    /**
     * Get settlementDate
     *
     * @return \DateTime
     */
    public function getSettlementDate()
    {
        return $this->settlementDate;
    }

    /**
     * Set unitPrice
     *
     * @param string $unitPrice
     *
     * @return Deal
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set commission
     *
     * @param string $commission
     *
     * @return Deal
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return string
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set managementRightsPrice
     *
     * @param string $managementRightsPrice
     *
     * @return Deal
     */
    public function setManagementRightsPrice($managementRightsPrice)
    {
        $this->managementRightsPrice = $managementRightsPrice;

        return $this;
    }

    /**
     * Get managementRightsPrice
     *
     * @return string
     */
    public function getManagementRightsPrice()
    {
        return $this->managementRightsPrice;
    }

    /**
     * Set managementRightsCommission
     *
     * @param string $managementRightsCommission
     *
     * @return Deal
     */
    public function setManagementRightsCommission($managementRightsCommission)
    {
        $this->managementRightsCommission = $managementRightsCommission;

        return $this;
    }

    /**
     * Get managementRightsCommission
     *
     * @return string
     */
    public function getManagementRightsCommission()
    {
        return $this->managementRightsCommission;
    }

    /**
     * Set otherUnitPurchasePrice
     *
     * @param string $otherUnitPurchasePrice
     *
     * @return Deal
     */
    public function setOtherUnitPurchasePrice($otherUnitPurchasePrice)
    {
        $this->otherUnitPurchasePrice = $otherUnitPurchasePrice;

        return $this;
    }

    /**
     * Get otherUnitPurchasePrice
     *
     * @return string
     */
    public function getOtherUnitPurchasePrice()
    {
        return $this->otherUnitPurchasePrice;
    }

    /**
     * Set otherUnitCommission
     *
     * @param string $otherUnitCommission
     *
     * @return Deal
     */
    public function setOtherUnitCommission($otherUnitCommission)
    {
        $this->otherUnitCommission = $otherUnitCommission;

        return $this;
    }

    /**
     * Get otherUnitCommission
     *
     * @return string
     */
    public function getOtherUnitCommission()
    {
        return $this->otherUnitCommission;
    }

    /**
     * Set totalCommissionExclGst
     *
     * @param string $totalCommissionExclGst
     *
     * @return Deal
     */
    public function setTotalCommissionExclGst($totalCommissionExclGst)
    {
        $this->totalCommissionExclGst = $totalCommissionExclGst;

        return $this;
    }

    /**
     * Get totalCommissionExclGst
     *
     * @return string
     */
    public function getTotalCommissionExclGst()
    {
        return $this->totalCommissionExclGst;
    }

    /**
     * Set totalCommissionGst
     *
     * @param string $totalCommissionGst
     *
     * @return Deal
     */
    public function setTotalCommissionGst($totalCommissionGst)
    {
        $this->totalCommissionGst = $totalCommissionGst;

        return $this;
    }

    /**
     * Get totalCommissionGst
     *
     * @return string
     */
    public function getTotalCommissionGst()
    {
        return $this->totalCommissionGst;
    }

    /**
     * Set netIncome
     *
     * @param string $netIncome
     *
     * @return Deal
     */
    public function setNetIncome($netIncome)
    {
        $this->netIncome = $netIncome;

        return $this;
    }

    /**
     * Get netIncome
     *
     * @return string
     */
    public function getNetIncome()
    {
        return $this->netIncome;
    }

    /**
     * Set multiplier
     *
     * @param string $multiplier
     *
     * @return Deal
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    /**
     * Get multiplier
     *
     * @return string
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }

    /**
     * Set officePercentage
     *
     * @param string $officePercentage
     *
     * @return Deal
     */
    public function setOfficePercentage($officePercentage)
    {
        $this->officePercentage = $officePercentage;

        return $this;
    }

    /**
     * Get officePercentage
     *
     * @return string
     */
    public function getOfficePercentage()
    {
        return $this->officePercentage;
    }

    /**
     * Set officeCommission
     *
     * @param string $officeCommission
     *
     * @return Deal
     */
    public function setOfficeCommission($officeCommission)
    {
        $this->officeCommission = $officeCommission;

        return $this;
    }

    /**
     * Get officeCommission
     *
     * @return string
     */
    public function getOfficeCommission()
    {
        return $this->officeCommission;
    }

    /**
     * Set officeSuper
     *
     * @param string $officeSuper
     *
     * @return Deal
     */
    public function setOfficeSuper($officeSuper)
    {
        $this->officeSuper = $officeSuper;

        return $this;
    }

    /**
     * Get officeSuper
     *
     * @return string
     */
    public function getOfficeSuper()
    {
        return $this->officeSuper;
    }

    /**
     * Set officeTotal
     *
     * @param string $officeTotal
     *
     * @return Deal
     */
    public function setOfficeTotal($officeTotal)
    {
        $this->officeTotal = $officeTotal;

        return $this;
    }

    /**
     * Get officeTotal
     *
     * @return string
     */
    public function getOfficeTotal()
    {
        return $this->officeTotal;
    }

    /**
     * Set officeGst
     *
     * @param string $officeGst
     *
     * @return Deal
     */
    public function setOfficeGst($officeGst)
    {
        $this->officeGst = $officeGst;

        return $this;
    }

    /**
     * Get officeGst
     *
     * @return string
     */
    public function getOfficeGst()
    {
        return $this->officeGst;
    }

    /**
     * Set vendorForwardingAddress
     *
     * @param string $vendorForwardingAddress
     *
     * @return Deal
     */
    public function setVendorForwardingAddress($vendorForwardingAddress)
    {
        $this->vendorForwardingAddress = $vendorForwardingAddress;

        return $this;
    }

    /**
     * Get vendorForwardingAddress
     *
     * @return string
     */
    public function getVendorForwardingAddress()
    {
        return $this->vendorForwardingAddress;
    }

    /**
     * Set conjunctionAgentAddress
     *
     * @param string $conjunctionAgentAddress
     *
     * @return Deal
     */
    public function setConjunctionAgentAddress($conjunctionAgentAddress)
    {
        $this->conjunctionAgentAddress = $conjunctionAgentAddress;

        return $this;
    }

    /**
     * Get conjunctionAgentAddress
     *
     * @return string
     */
    public function getConjunctionAgentAddress()
    {
        return $this->conjunctionAgentAddress;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Deal
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set listingAgent
     *
     * @param \AppBundle\Entity\Agent $listingAgent
     *
     * @return Deal
     */
    public function setListingAgent(\AppBundle\Entity\Agent $listingAgent = null)
    {
        $this->listingAgent = $listingAgent;

        return $this;
    }

    /**
     * Get listingAgent
     *
     * @return \AppBundle\Entity\Agent
     */
    public function getListingAgent()
    {
        return $this->listingAgent;
    }

    /**
     * Add staffCommission
     *
     * @param \AppBundle\Entity\StaffCommission $staffCommission
     *
     * @return Deal
     */
    public function addStaffCommission(\AppBundle\Entity\StaffCommission $staffCommission)
    {
        $this->staffCommissions[] = $staffCommission;

        return $this;
    }

    /**
     * Remove staffCommission
     *
     * @param \AppBundle\Entity\StaffCommission $staffCommission
     */
    public function removeStaffCommission(\AppBundle\Entity\StaffCommission $staffCommission)
    {
        $this->staffCommissions->removeElement($staffCommission);
    }

    /**
     * Get staffCommissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStaffCommissions()
    {
        return $this->staffCommissions;
    }

    /**
     * Set confirmationEmailSentDate
     *
     * @param \DateTime $confirmationEmailSentDate
     *
     * @return Deal
     */
    public function setConfirmationEmailSentDate($confirmationEmailSentDate)
    {
        $this->confirmationEmailSentDate = $confirmationEmailSentDate;

        return $this;
    }

    /**
     * Get confirmationEmailSentDate
     *
     * @return \DateTime
     */
    public function getConfirmationEmailSentDate()
    {
        return $this->confirmationEmailSentDate;
    }

    /**
     * Set contractsSignedDate
     *
     * @param \DateTime $contractsSignedDate
     *
     * @return Deal
     */
    public function setContractsSignedDate($contractsSignedDate)
    {
        $this->contractsSignedDate = $contractsSignedDate;

        return $this;
    }

    /**
     * Get contractsSignedDate
     *
     * @return \DateTime
     */
    public function getContractsSignedDate()
    {
        return $this->contractsSignedDate;
    }

    /**
     * Set submittedToSolicitorsDate
     *
     * @param \DateTime $submittedToSolicitorsDate
     *
     * @return Deal
     */
    public function setSubmittedToSolicitorsDate($submittedToSolicitorsDate)
    {
        $this->submittedToSolicitorsDate = $submittedToSolicitorsDate;

        return $this;
    }

    /**
     * Get submittedToSolicitorsDate
     *
     * @return \DateTime
     */
    public function getSubmittedToSolicitorsDate()
    {
        return $this->submittedToSolicitorsDate;
    }

    /**
     * Set property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return Deal
     */
    public function setProperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \AppBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set buyer
     *
     * @param \AppBundle\Entity\Buyer $buyer
     *
     * @return Deal
     */
    public function setBuyer(\AppBundle\Entity\Buyer $buyer = null)
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * Get buyer
     *
     * @return \AppBundle\Entity\Buyer
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set legalDiligenceSolicitor
     *
     * @param string $legalDiligenceSolicitor
     *
     * @return Deal
     */
    public function setLegalDiligenceSolicitor($legalDiligenceSolicitor)
    {
        $this->legalDiligenceSolicitor = $legalDiligenceSolicitor;

        return $this;
    }

    /**
     * Get legalDiligenceSolicitor
     *
     * @return string
     */
    public function getLegalDiligenceSolicitor()
    {
        return $this->legalDiligenceSolicitor;
    }

    /**
     * Set totalCommission
     *
     * @param string $totalCommission
     *
     * @return Deal
     */
    public function setTotalCommission($totalCommission)
    {
        $this->totalCommission = $totalCommission;

        return $this;
    }

    /**
     * Get totalCommission
     *
     * @return string
     */
    public function getTotalCommission()
    {
        return $this->totalCommission;
    }

    /**
     * Set offerAcceptedDate
     *
     * @param \DateTime $offerAcceptedDate
     *
     * @return Deal
     */
    public function setOfferAcceptedDate($offerAcceptedDate)
    {
        $this->offerAcceptedDate = $offerAcceptedDate;

        return $this;
    }

    /**
     * Get offerAcceptedDate
     *
     * @return \DateTime
     */
    public function getOfferAcceptedDate()
    {
        return $this->offerAcceptedDate;
    }

    /**
     * Set paidAndFinalisedDate
     *
     * @param \DateTime $paidAndFinalisedDate
     *
     * @return Deal
     */
    public function setPaidAndFinalisedDate($paidAndFinalisedDate)
    {
        $this->paidAndFinalisedDate = $paidAndFinalisedDate;

        return $this;
    }

    /**
     * Get paidAndFinalisedDate
     *
     * @return \DateTime
     */
    public function getPaidAndFinalisedDate()
    {
        return $this->paidAndFinalisedDate;
    }
}
