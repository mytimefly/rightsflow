<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="property_files")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PropertyFile extends BaseFile {
    /**
     * @ORM\ManyToOne(targetEntity="Property", inversedBy="files")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     **/
    private $property;

    /**
     * Set property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return PropertyFile
     */
    public function setProperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \AppBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/property';
    }

    /**
     * @return string
     */
    public function getUrl() {
        return '/'.$this->getUploadDir().'/'.$this->getPath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function deleteFile() {
        if ($file = $this->getUploadRootDir().'/'.$this->getPath()) {
            return (file_exists($file) && unlink($file));
        }
    }
}
