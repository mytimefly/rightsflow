<?php

namespace AppBundle\Form;

use AppBundle\Entity\Deal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\PropertyRepository;

class DealType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('property', EntityType::class, array(
                'class' => 'AppBundle:Property',
                'choice_label' => 'title',
                'placeholder' => '- select property -',
                'required' => false,
                // only display properties that are open for deals
                'query_builder' => function(\Doctrine\ORM\EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('p')
                        ->leftJoin('AppBundle:Deal', 'd','WITH','d.property = p')
                        ->where('p.archived = FALSE')
                        ->andWhere('d.id IS NULL OR d.status IN (?0)')
                        ->orWhere('p = ?1') // need to still display this deal's property for edit form
                        ->orderBy('p.title', 'ASC')
                        ->setParameters(array(
                            Deal::$statuses_open,
                            $options['data']->getProperty()
                        ));
                }
            ))
            ->add('buyer', EntityType::class, array(
                'class' => 'AppBundle:Buyer',
                'choice_label' => 'name',
                'placeholder' => '- select buyer -',
                'required' => false,
                'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                    return $er->createQueryBuilder('b')
                        ->addOrderBy('b.firstName', 'ASC')
                        ->addOrderBy('b.lastName', 'ASC');
                },
            ))
            ->add('listingAgent', EntityType::class, array(
                'class' => 'AppBundle:Agent',
                'choice_label' => 'name',
                'placeholder' => '- select agent -',
                'required' => false
            ))
            ->add('status', ChoiceType::class, [
                    'choices' => [
                        'Under Contract' => Deal::STATUS_UNDER_CONTRACT,
                        'Accounting Due Dilligence' => Deal::STATUS_ACCOUNTING_DUE_DILIGENCE,
                        'Legal Due Diligence' => Deal::STATUS_LEGAL_DUE_DILIGENCE,
                        'Request For Extension' => Deal::STATUS_REQUEST_FOR_EXTENSION,
                        'Unconditional' => Deal::STATUS_UNCONDITIONAL,
                        'Settled' => Deal::STATUS_SETTLED,
                        'Not Proceeding' => Deal::STATUS_NOT_PROCEEDING
                    ],
                    'choices_as_values' => true,
                    'placeholder' => '- Select -'
                ]
            )
            ->add('saleSource', ChoiceType::class, [
                    'choices' => [
                        'Website Enquiry' => Deal::SALE_SOURCE_WEB_ENQUIRY,
                        'Approached by Staff/Agent/Contractor' => Deal::SALE_SOURCE_STAFF,
                        'Referral' => Deal::SALE_SOURCE_REFERRAL,
                        'Onsite Manager' => Deal::SALE_SOURCE_ONSITE_MANAGER,
                        'Other' => Deal::SALE_SOURCE_OTHER
                    ],
                    'choices_as_values' => true,
                    'placeholder' => '- Select -',
                    'required' => false
                ])
            ->add('form6', ChoiceType::class, [
                    'choices' => [
                        'Yes' => 1,
                        'No' => 0,
                    ],
                    'choices_as_values' => true,
                    'placeholder' => '- Select -',
                    'required' => false
                ])
            ->add('confirmationEmailSentDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('offerAcceptedDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('submittedToSolicitorsDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('contractsSignedDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('saleDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('deposit', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('depositDueDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('balance', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('balanceDueDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('financeBank')
            ->add('financeDueDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('accountant')
            ->add('accountantDiligenceDueDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('legalDiligenceSolicitor')
            ->add('legalDiligenceDueDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('bodyCorpMeetingDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('committeeAgmDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('committeeEgmDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('settlementDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])

            ->add('unitPrice', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('commission', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('managementRightsPrice', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('managementRightsCommission', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('otherUnitPurchasePrice', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('otherUnitCommission', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('totalCommissionExclGst', MoneyType::class, array('currency'=>'USD', 'required' => false, 'attr' => ['readonly' => true]))
            ->add('totalCommissionGst', MoneyType::class, array('currency'=>'USD', 'required' => false, 'attr' => ['readonly' => true]))
            ->add('totalCommission', MoneyType::class, array('currency'=>'USD', 'required' => false, 'attr' => ['readonly' => true]))
            ->add('netIncome', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('multiplier', NumberType::class, array('required' => false, 'attr' => ['readonly' => true]))
            ->add('officePercentage', NumberType::class, array('required' => false))
            ->add('officeCommission', MoneyType::class, array('currency'=>'USD', 'required' => false, 'attr' => ['readonly' => true]))
            ->add('officeSuper', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('officeTotal', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('officeGst', MoneyType::class, array('currency'=>'USD', 'required' => false))
            ->add('vendorForwardingAddress')
            ->add('conjunctionAgentAddress')
            ->add('paidAndFinalisedDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('comments', null, ['required' => false, 'empty_data' => ''])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Deal'
        ));
    }
}
