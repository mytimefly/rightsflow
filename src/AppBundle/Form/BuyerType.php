<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuyerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',null,['required'=>true])
            ->add('lastName')
            ->add('emailAddress',EmailType::class, ['required'=>true])
            ->add('phone')
            ->add('mobile')
            ->add('fax')
            ->add('company')
            ->add('address')
            ->add('suburb')
            ->add('state')
            ->add('postcode')
            ->add('emailConsent')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Buyer'
        ));
    }
}
