<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BuyerNoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $buyerId = null;

        $builder
            ->add('agent', EntityType::class, array(
                'class' => 'AppBundle:Agent',
                'choice_label' => 'name',
                'placeholder' => '- select agent -',
                'required' => true
            ))
            ->add('title')
            ->add('visitDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('details');

        if (isset($_REQUEST['buyerId'])) {
            $buyerId = $_REQUEST['buyerId'];
        }

        if ($buyerId) {
            $builder->add('buyer', HiddenType::class, array(
                    'mapped' => false,
                    'data' => $buyerId
                ));
        }
        else {
            $builder
                ->add('buyer', EntityType::class, array(
                    'class' => 'AppBundle:Buyer',
                    'choice_label' => 'name',
                    'placeholder' => '- select buyer -',
                    'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                        return $er->createQueryBuilder('b')
                            ->addOrderBy('b.firstName', 'ASC')
                            ->addOrderBy('b.lastName', 'ASC');
                    },
                ));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BuyerNote'
        ));
    }
}
