<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Property;

class PropertyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, ['choices' => array_flip(Property::$statuses)])
            ->add('listingDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('agent', EntityType::class, array(
                'class' => 'AppBundle:Agent',
                'choice_label' => 'name',
                'placeholder' => '- select agent -',
                'required' => false
            ))
            ->add('listingType', ChoiceType::class, [
                'choices' => [
                    'Exclusive' => Property::LISTING_TYPE_EXCL,
                    'Open' => Property::LISTING_TYPE_OPEN,
                    'Sole' => Property::LISTING_TYPE_SOLE,
                    'Joint' => Property::LISTING_TYPE_JOINT,
                ],
                'choices_as_values' => true,
                'placeholder' => '- Select -'
            ])
            ->add('listingExpiry', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ],
                'required' => false])
            ->add('listingComplexType', ChoiceType::class, [
                'choices' => [
                    'Retirement' => Property::COMPLEX_TYPE_RETIREMENT,
                    'Resort/Holiday' => Property::COMPLEX_TYPE_RESORT_HOLIDAY,
                    'Permanent' => Property::COMPLEX_TYPE_PERMANENT,
                    'Off The Plan' => Property::COMPLEX_TYPE_OFF_THE_PLAN,
                    'Corporate' => Property::COMPLEX_TYPE_CORPORATE,
                    'Student Accommodation' => Property::COMPLEX_TYPE_STUDENT_ACCOM
                ],
                'choices_as_values' => true,
                'placeholder' => '- Select -'
            ])
            ->add('primaryLocation')
            ->add('secondaryLocation')
            ->add('alertAllBuyers')
            ->add('address')
            ->add('suburb')
            ->add('state')
            ->add('postcode')
            ->add('planNumber', null, ['required' => false, 'empty_data' => ''])
            ->add('parish', null, ['required' => false, 'empty_data' => ''])
            ->add('county', null, ['required' => false, 'empty_data' => ''])
            ->add('lastSoldDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker', 'required' => false
                ],
                'required' => false])
            ->add('featured')
            ->add('archived')
            ->add('showOnWebsite')
            ->add('showOnExternalWebsites')
            ->add('showInEmailBlasts')

            // Contact Info
            ->add('phone')
            ->add('fax')
            ->add('mobile')
            ->add('email', EmailType::class, array(
                'required' => false
            ))
            ->add('receiveMarketingEmails')
            ->add('primaryContactName')
            ->add('secondaryContactName')
            ->add('managerUnitNumber')
            ->add('websiteUrl', UrlType::class, array(
                'required' => false
            ))
            ->add('uBDReference', null, ['label' => 'UBD Reference'])

            // LETTING
            ->add('yearBuilt', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('totalNumUnits', null, [
                'label' => 'Total Unit Count',
                'attr' => array('readonly' => true)
            ])
            ->add('numPermanentUnits', null, [
                'label' => 'Permanent'
            ])
            ->add('numHolidayUnits', null, [
                'label' => 'Holiday'
            ])
            ->add('numCorporateUnit', null, [
                'label' => 'Corporate'
            ])
            ->add('totalNumLettingUnits', null, [
                'label' => 'Total',
                'attr' => array('readonly' => true)
            ])
            ->add('numOutsideLettingLockUp', null, [
                'label' => 'Lock Up'
            ])
            ->add('numOutsideLettingOutsideAgents', null, [
                'label' => 'External Agent'
            ])
            ->add('numOutsideLettingSelfManaged', null, [
                'label' => 'Self Managed'
            ])
            ->add('numOutsideLettingOwnerOccupied', null, [
                'label' => 'Owner Occupied'
            ])
            ->add('totalNumOutsideLettingUnits', null, [
                'label' => 'Total',
                'attr' => array('readonly' => true)
            ])
            ->add('numMultipleUnitOwnersInComplex', null, [
                'label' => 'Multiple Unit Owners In Complex'
            ])

            // OFFICE
            ->add('officeType', ChoiceType::class, [
                'choices' => [
                    Property::$office_types[Property::OFFICE_TYPE_EXCLUSIVE_USE] => Property::OFFICE_TYPE_EXCLUSIVE_USE,
                    Property::$office_types[Property::OFFICE_TYPE_ON_TITLE] => Property::OFFICE_TYPE_ON_TITLE,
                ],
                'choices_as_values' => true,
                'placeholder' => '- Select -',
                'required' => false
            ])
            ->add('managerUnitSize', ChoiceType::class, [
                'label' => 'Unit Size',
                'choices' => [
                    '1 Bedroom' => 1,
                    '2 Bedroom' => 2,
                    '3 Bedroom' => 3,
                    '4 Bedroom' => 4,
                    '5 Bedroom' => 5,
                    'N\A' => 0
                ],
                'placeholder' => '- Select -',
                'required' => false
            ])
            ->add('managerUnitDescription', null, [
                'label' => 'Unit Description'
            ])
            ->add('storage')
            ->add('officeReception')
            ->add('officeHours')
            ->add('officeSoftware')
            ->add('module', ChoiceType::class, [
                'choices' => [
                    'Accom' => Property::MODULE_ACCOM,
                    'Standard' => Property::MODULE_STANDARD,
                    'Other' => Property::MODULE_OTHER,
                ],
                'choices_as_values' => true,
                'placeholder' => '- Select -',
                'required' => false]
            )
            ->add('agreementTerm', null, ['label' => 'Agreement Term (months)'])
            ->add('yearsToRun')
            ->add('bodyCorpSecretary', null, ['label' => 'Secretary'])
            ->add('bodyCorpManager', null, ['label' => 'Manager'])
            ->add('lettingAgreementAvailable')
            ->add('caretakingAgreementAvailable')


            // Tariffs
            ->add('permanentTariff')
            ->add('corporateTariff')
            ->add('holidayTariff')


            // Financials
            ->add('bodycorporateSalary')
            ->add('salaryAnniversary')
            ->add('bodycorporateSalaryReview')
            ->add('managerUnitBodyCorporateFees')
            ->add('managerUnitRates')
            ->add('sinkingFundBalance')
            ->add('managerUnitPrice')
            ->add('managementRightsPrice')
            ->add('totalPrice')
            ->add('priceDisplayAlt')
            ->add('netProfit')
            ->add('registeredProprietorManagersUnit')
            ->add('registeredProprietorRightsBusiness')
            ->add('sellerSolicitor')
            ->add('sellerAccountant')


            // Publishing
            ->add('title')
            ->add('content', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array(
                'required' => false,
                'attr' => array('class' => 'tinymce')
            ))
            ->add('contentHidden')
            ->add('uploadedFiles', \Symfony\Component\Form\Extension\Core\Type\FileType::class, array(
                'multiple' => true,
                'data_class' => null,
                'required' => false,
                'label' => 'Upload images'
            ));


        // add checkboxes for files so we can select them to delete
        foreach ($options['data']->getFiles() AS $file) {
            $builder->add(
                'deleteFile'.$file->getId(), \Symfony\Component\Form\Extension\Core\Type\CheckboxType::class, array(
                'value' => $file->getId(),
                'data_class' => null,
                'required' => false,
                'mapped' => false,
                'label' => 'Delete image'
            ));
        }

        // add checkboxes for features so we can select them to delete
        $builder->add('features', EntityType::class, array(
        'class' => 'AppBundle:PropertyFeature',
        'choice_label' => 'name',
        'required' => false,
        'multiple' => true,
        'expanded' => true,
        // only display properties that are open for deals
        'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
            return $er->createQueryBuilder('pf')
                ->orderBy('pf.name', 'ASC');
        }));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Property'
        ));
    }
}
