<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PropertyNoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = null;

        $builder
            ->add('agent', EntityType::class, array(
                'class' => 'AppBundle:Agent',
                'choice_label' => 'name',
                'placeholder' => '- select agent -',
                'required' => true
            ))
            ->add('title')
            ->add('visitDate', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
            ->add('details');

        if (isset($_REQUEST['propertyId'])) {
            $propertyId = $_REQUEST['propertyId'];
        }

        if ($propertyId) {
            $builder->add('property', HiddenType::class, array(
                    'mapped' => false,
                    'data' => $propertyId
                ));
        }
        else {
            $builder
                ->add('property', EntityType::class, array(
                    'class' => 'AppBundle:Property',
                    'choice_label' => 'title',
                    'placeholder' => '- select property -',
                ));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PropertyNote'
        ));
    }
}
