<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AgentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('profilePicFile', VichImageType::class, array(
                    'required'      => false,
                    'allow_delete'  => true, // not mandatory, default is true
                    'download_link' => false,
                ))
            ->add('emailAddress',EmailType::class)
            ->add('phone')
            ->add('mobile')
            ->add('fax')
            ->add('company')
            ->add('address')
            ->add('suburb')
            ->add('state')
            ->add('postcode')
            ->add('licenseNumber')
            ->add('licenseExpiry', null,
                ['widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'attr'   =>  [
                    'class'   => 'datepicker'
                ]])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Agent'
        ));
    }
}
