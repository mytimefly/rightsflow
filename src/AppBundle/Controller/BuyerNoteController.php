<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\BuyerNote;
use AppBundle\Form\BuyerNoteType;

/**
 * BuyerNote controller.
 *
 * @Route("/buyer-notes")
 */
class BuyerNoteController extends Controller
{
    /**
     * Lists all BuyerNote entities.
     *
     * @Route("/", name="buyernote_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $buyerNotes = $em->getRepository('AppBundle:BuyerNote')->findAll();

        return $this->render('buyernote/index.html.twig', array(
            'buyerNotes' => $buyerNotes,
        ));
    }

    /**
     * Creates a new BuyerNote entity.
     *
     * @Route("/new", name="buyernote_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $buyerNote = new BuyerNote();
        $form = $this->createForm('AppBundle\Form\BuyerNoteType', $buyerNote, [
            'action' => $this->generateUrl('buyernote_new')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buyerNote);
            $em->flush();

            // check if ajax
            if ($request->isXmlHttpRequest()) {
                $data = array(
                    'id' => $buyerNote->getId(),
                    'title' => $buyerNote->getTitle(),
                    'agent' => $buyerNote->getAgent()->getName(),
                    'details' => $buyerNote->getDetails(),
                    'visitDate' => $buyerNote->getVisitDate()->format('d M Y'),
                    'createdAt' => $buyerNote->getCreatedAt()->format('d M Y g:i a')
                );
                return new JsonResponse($data, 200);
            }
            else {
                return $this->redirectToRoute('buyernote_show', array('id' => $buyerNote->getId()));
            }
        }

        return $this->render('buyernote/new.html.twig', array(
            'buyerNote' => $buyerNote,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BuyerNote entity.
     *
     * @Route("/{id}", name="buyernote_show")
     * @Method("GET")
     */
    public function showAction(BuyerNote $buyerNote)
    {
        $deleteForm = $this->createDeleteForm($buyerNote);

        return $this->render('buyernote/show.html.twig', array(
            'buyerNote' => $buyerNote,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BuyerNote entity.
     *
     * @Route("/{id}/edit", name="buyernote_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BuyerNote $buyerNote)
    {
        $deleteForm = $this->createDeleteForm($buyerNote);
        $editForm = $this->createForm('AppBundle\Form\BuyerNoteType', $buyerNote, [
            'action' => $this->generateUrl('buyernote_edit', ['id' => $buyerNote->getId()]),
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buyerNote);
            $em->flush();

            // check if ajax
            if ($request->isXmlHttpRequest()) {
                $data = array(
                    'id' => $buyerNote->getId(),
                    'title' => $buyerNote->getTitle(),
                    'agent' => $buyerNote->getAgent()->getName(),
                    'details' => $buyerNote->getDetails(),
                    'visitDate' => $buyerNote->getVisitDate()->format('d M Y'),
                    'createdAt' => $buyerNote->getCreatedAt()->format('d M Y g:i a')
                );
                return new JsonResponse($data, 200);
            }
            else {
                return $this->redirectToRoute('buyernote_edit', array('id' => $buyerNote->getId()));
            }
        }

        return $this->render('buyernote/edit.html.twig', array(
            'buyerNote' => $buyerNote,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a BuyerNote entity.
     *
     * @Route("/{id}", name="buyernote_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BuyerNote $buyerNote)
    {
        $form = $this->createDeleteForm($buyerNote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($buyerNote);
            $em->flush();
        }

        return $this->redirectToRoute('buyernote_index');
    }

    /**
     * Creates a form to delete a BuyerNote entity.
     *
     * @param BuyerNote $buyerNote The BuyerNote entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BuyerNote $buyerNote)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('buyernote_delete', array('id' => $buyerNote->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
