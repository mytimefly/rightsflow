<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Agent;
use AppBundle\Entity\Buyer;
use AppBundle\Form\BuyerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use AppBundle\Entity\Property;
use JMS;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Agent controller.
 *
 * @Route("/api")
 */
class ApiController extends Controller
{
    protected $serializer;

    public function __construct() {
        $this->serializer = JMS\Serializer\SerializerBuilder::create()->build();
    }

    /**
     * Get all locations for properties
     * @Route("/property/locations/", name="api_property_locations_list")
     * @Method("GET")
     */
    public function getPropertyLocationsAction(Request $request) {
        $propertySvc = $this->get('property.service');
        $locations = $propertySvc->getLocations();

        return new Response($this->serializer->serialize($locations, 'json'));
    }

    /**
     * Lists all Property entities.
     *
     * @Route("/property/", name="api_property_list")
     * @Method("GET")
     */
    public function listPropertiesAction(Request $request)
    {
        $propertySvc = $this->get('property.service');
        $data = $propertySvc->getProperties();

        return new Response($this->serializer->serialize($data, 'json'));
    }

    /**
     * Finds and displays a Property entity.
     *
     * @Route("/property/{id}", name="api_property_show")
     * @Method("GET")
     */
    public function showPropertyAction(Property $property)
    {
        return new Response($this->serializer->serialize($property, 'json'));
    }

    /**
     * @Route("/buyer", name="api_buyer_new")
     * @Method("POST")
     */
    public function addBuyerAction(Request $request) {
        // setup form
        $buyer = new Buyer();
        $form = $this->createForm('AppBundle\Form\BuyerType', $buyer, array('csrf_protection' => false));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $buyerService = $this->get('buyer.service');
            if ($newBuyer = $buyerService->addBuyer($buyer)) {
                $response = [
                    'success' => true,
                    'buyer' => false
                ];
            }
            else {
                $response = [
                  'success' => false,
                    'buyer' => $buyer
                ];
            }

            // return the response
            return new Response($this->serializer->serialize($response, 'json'));
        }
        else {
            $response['errors'] = (string) $form->getErrors(true, false);
        }

        $response['success'] = false;

        return new Response($this->serializer->serialize($response, 'json'));
    }
}
