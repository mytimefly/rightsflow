<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\PropertyNote;
use AppBundle\Form\PropertyNoteType;

/**
 * PropertyNote controller.
 *
 * @Route("/property-notes")
 */
class PropertyNoteController extends Controller
{
    /**
     * Lists all PropertyNote entities.
     *
     * @Route("/", name="propertynote_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $propertyNotes = $em->getRepository('AppBundle:PropertyNote')->findAll();

        return $this->render('propertynote/index.html.twig', array(
            'propertyNotes' => $propertyNotes,
        ));
    }

    /**
     * Creates a new PropertyNote entity.
     *
     * @Route("/new", name="propertynote_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $propertyNote = new PropertyNote();
        $form = $this->createForm('AppBundle\Form\PropertyNoteType', $propertyNote, array(
            'action' => $this->generateUrl('propertynote_new'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // set property
            if ($propertyId = $request->query->get('propertyId')) {
                $property = $this->get('doctrine')
                    ->getRepository('AppBundle:Property')
                    ->find($propertyId);
                $property->addNote($propertyNote);

                $propertyNote->setProperty($property);

                $em->persist($property);
            }

            $em->persist($propertyNote);

            $em->flush();

            // check if ajax
            if ($request->isXmlHttpRequest()) {
                $data = array(
                    'id' => $propertyNote->getId(),
                    'title' => $propertyNote->getTitle(),
                    'agent' => $propertyNote->getAgent()->getName(),
                    'details' => $propertyNote->getDetails(),
                    'visitDate' => $propertyNote->getVisitDate()->format('d M Y'),
                    'createdAt' => $propertyNote->getCreatedAt()->format('d M Y g:i a')
                );
                return new JsonResponse($data, 200);
            }
            else {
                return $this->redirectToRoute('propertynote_show', array('id' => $propertyNote->getId()));
            }
        }

        return $this->render('propertynote/new.html.twig', array(
            'propertyNote' => $propertyNote,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PropertyNote entity.
     *
     * @Route("/{id}", name="propertynote_show")
     * @Method("GET")
     */
    public function showAction(PropertyNote $propertyNote)
    {
        $deleteForm = $this->createDeleteForm($propertyNote);

        return $this->render('propertynote/show.html.twig', array(
            'propertyNote' => $propertyNote,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PropertyNote entity.
     *
     * @Route("/{id}/edit", name="propertynote_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PropertyNote $propertyNote)
    {
        $deleteForm = $this->createDeleteForm($propertyNote);
        $editForm = $this->createForm('AppBundle\Form\PropertyNoteType', $propertyNote, [
            'action' => $this->generateUrl('propertynote_edit', ['id' => $propertyNote->getId()]),
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($propertyNote);
            $em->flush();

            // check if ajax
            if ($request->isXmlHttpRequest()) {
                $data = array(
                    'id' => $propertyNote->getId(),
                    'title' => $propertyNote->getTitle(),
                    'agent' => $propertyNote->getAgent()->getName(),
                    'details' => $propertyNote->getDetails(),
                    'visitDate' => $propertyNote->getVisitDate()->format('d M Y'),
                    'createdAt' => $propertyNote->getCreatedAt()->format('d M Y g:i a')
                );
                return new JsonResponse($data, 200);
            }
            else {
                return $this->redirectToRoute('propertynote_show', array('id' => $propertyNote->getId()));
            }
        }

        return $this->render('propertynote/edit.html.twig', array(
            'propertyNote' => $propertyNote,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PropertyNote entity.
     *
     * @Route("/{id}", name="propertynote_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PropertyNote $propertyNote)
    {
        $form = $this->createDeleteForm($propertyNote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($propertyNote);
            $em->flush();
        }

        return $this->redirectToRoute('propertynote_index');
    }

    /**
     * Creates a form to delete a PropertyNote entity.
     *
     * @param PropertyNote $propertyNote The PropertyNote entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PropertyNote $propertyNote)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('propertynote_delete', array('id' => $propertyNote->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
