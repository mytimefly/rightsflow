<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Property;
use AppBundle\Entity\PropertyFeature;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Property controller.
 *
 * @Route("/property")
 */
class PropertyController extends Controller
{
    protected $serializer;

    public function __construct() {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function($o){
            return $o->getId();
        });

        $this->serializer = new Serializer([$normalizer], $encoders);
    }

    /**
     * Lists all Property entities.
     *
     * @Route("/", name="property_list")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $propertySvc = $this->get('property.service');

        $em = $this->getDoctrine()->getManager();

        $propertySvc = $this->get('property.service');
        $properties = $propertySvc->getProperties();
        $locations = $propertySvc->getLocations();
        $statuses = Property::$statuses;
        // default status to For Sale
        $status = ($request->get('status')) ? $request->get('status') : Property::STATUS_ACTIVE;

        return $this->render('property/index.html.twig', array(
            'properties' => $properties,
            'locations' => $locations,
            'statuses' => $statuses,
            'selectedLocation' => $request->get('location'),
            'minPrice' => $request->get('min-price'),
            'maxPrice' => $request->get('max-price'),
            'minBeds' => $request->get('min-beds'),
            'minIncome' => $request->get('min-income'),
            'selectedStatus' => $status,
            'q' => (isset($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : ''
        ));
    }

    /**
     * Lists all Property entities.
     *
     * @Route("/fieldlist", name="property_fieldlist")
     * @Method("GET")
     */
    public function fieldlistAction(Request $request)
    {
        $propertySvc = $this->get('property.service');
        $em = $this->getDoctrine()->getManager();

        $propertySvc = $this->get('property.service');
        $properties = $propertySvc->getProperties();
        $locations = $propertySvc->getLocations();
        $statuses = Property::$statuses;
        // default status to For Sale
        $status = ($request->get('status')) ? $request->get('status') : Property::STATUS_ACTIVE;
        $priceLabel = '';
        $minPrice = $request->get('min-price');
        $maxPrice = $request->get('max-price');
        $location = $request->get('location');

        // determin priceLabel
        if ($minPrice && $maxPrice) {
            $priceLabel = $minPrice.' - '.$maxPrice;
        }
        elseif ($minPrice) {
            $priceLabel = $minPrice.' and above';
        }
        elseif ($maxPrice) {
            $priceLabel = 'Up to '.$maxPrice;
        }
        else {
            $priceLabel = 'Any';
        }

        // determine locationLabel
        $locationLabel = (!empty($location)) ? $location : 'All';


        return $this->render('property/fieldlist.html.twig', array(
            'properties' => $properties,
            'locations' => $locations,
            'statuses' => $statuses,
            'location' => $locationLabel,
            'price' => $priceLabel,
            'minBeds' => $request->get('min-beds') ? $request->get('min-beds') : 'All',
            'minIncome' => $request->get('min-income') ? $request->get('min-income') : 'Any',
            'selectedStatus' => $status
        ));
    }

    /**
     * Lists all Property entities.
     *
     * @Route("/handout", name="property_handout")
     * @Method("GET")
     */
    public function handoutAction(Request $request)
    {
        $propertySvc = $this->get('property.service');
        $em = $this->getDoctrine()->getManager();

        $propertySvc = $this->get('property.service');
        $properties = $propertySvc->getProperties();
        $locations = $propertySvc->getLocations();
        $statuses = Property::$statuses;
        // default status to For Sale
        $status = ($request->get('status')) ? $request->get('status') : Property::STATUS_ACTIVE;
        $priceLabel = '';
        $minPrice = $request->get('min-price');
        $maxPrice = $request->get('max-price');
        $location = $request->get('location');

        // determin priceLabel
        if ($minPrice && $maxPrice) {
            $priceLabel = $minPrice.' - '.$maxPrice;
        }
        elseif ($minPrice) {
            $priceLabel = $minPrice.' and above';
        }
        elseif ($maxPrice) {
            $priceLabel = 'Up to '.$maxPrice;
        }
        else {
            $priceLabel = 'Any';
        }

        // determine locationLabel
        $locationLabel = (!empty($location)) ? $location : 'All';


        return $this->render('property/handout.html.twig', array(
            'properties' => $properties,
            'locations' => $locations,
            'statuses' => $statuses,
            'location' => $locationLabel,
            'price' => $priceLabel,
            'minBeds' => $request->get('min-beds') ? $request->get('min-beds') : 'All',
            'minIncome' => $request->get('min-income') ? $request->get('min-income') : 'Any',
            'selectedStatus' => $status
        ));
    }

    /**
     * Creates a new Property entity.
     *
     * @Route("/new", name="property_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $property = new Property();
        $form = $this->createForm('AppBundle\Form\PropertyType', $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();

            return $this->redirectToRoute('property_edit', array('id' => $property->getId()));
        }

        return $this->render('property/new.html.twig', array(
            'property' => $property,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a Property entity.
     *
     * @Route("/{id}.{_format}",
     *  defaults = {"_format"="json"},
     *  requirements = { "_format" = "json"},
     *  name="property_show"
     * )
     * @Method("GET")
     */
    public function showAction(Property $property, $_format)
    {
        if ($_format == 'json') {
            return new Response($this->serializer->serialize($property, 'json'));
        }

        // otherwise display show page as normal
        $deleteForm = $this->createDeleteForm($property);

        return $this->render('property/show.html.twig', array(
            'property' => $property,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Property entity.
     *
     * @Route("/{id}/edit", name="property_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Property $property)
    {
        $deleteForm = $this->createDeleteForm($property);
        $editForm = $this->createForm('AppBundle\Form\PropertyType', $property);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // loop property files
            foreach ($property->getFiles() AS $file) {
                // if one was selected to delete - delete it
                if (isset($editForm['deleteFile'.$file->getId()]) &&
                    $set = $editForm['deleteFile'.$file->getId()]->getData()) {
                    $property->removeFile($file);
                    $em->remove($file);
                }
            }

            // save property
            $em->persist($property);
            $em->flush();

            return $this->redirectToRoute('property_edit', array('id' => $property->getId()));
        }

        return $this->render('property/edit.html.twig', array(
            'property' => $property,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Property entity.
     *
     * @Route("/{id}", name="property_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Property $property)
    {
        $form = $this->createDeleteForm($property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($property);
            $em->flush();
        }

        return $this->redirectToRoute('property_list');
    }

    /**
     * Creates a form to delete a Property entity.
     *
     * @param Property $property The Property entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Property $property)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('property_delete', array('id' => $property->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
