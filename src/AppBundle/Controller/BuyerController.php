<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Buyer;
use AppBundle\Form\BuyerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Buyer controller.
 *
 * @Route("/buyer")
 */
class BuyerController extends Controller
{
    protected $serializer;

    public function __construct() {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function($o){
            return $o->getId();
        });

        $this->serializer = new Serializer([$normalizer], $encoders);
    }

    /**
     * @Route("/import", name="buyer_import")
     */
    public function importAction(Request $request) {
        $importCount = 0;
        $errors = 0;

        $form = $this->createFormBuilder()
            ->add('file', \Symfony\Component\Form\Extension\Core\Type\FileType::class, array('label' => 'File to Submit'))
            ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, array('attr' => array('class' => 'btn btn-primary')))
            ->getForm();

        $form->handleRequest($request);

        // Check if we are posting stuff
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Get file
            $file = $form->get('file')->getData();

            // move file for processing
            $newFilepath = $this->container->getParameter('kernel.root_dir').'/../web/uploads/';
            $newFilename = 'buyer-import.csv';
            $file->move($newFilepath,$newFilename);

            $handle = fopen($newFilepath.$newFilename,"r");
            while (($buffer = fgetcsv($handle, 4096)) !== false) {
                list($name, $email, $phone) = $buffer;

                $buyer = new Buyer();

                // split name
                $names = explode(' ', $name, 2);

                if (!empty($names[0]) && $firstName = $names[0]) {
                    $buyer->setFirstName(utf8_encode($firstName));
                }

                if (!empty($names[1]) && $lastName = $names[1]) {
                    $buyer->setLastName(utf8_encode($lastName));
                }

                if (!empty($email)) {
                    $buyer->setEmailAddress($email);
                }

                if (!empty($phone)) {
                    $buyer->setPhone($phone);
                }
                $em->persist($buyer);
                $em->flush();
                $importCount++;

            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }

            // delete file
            unlink($newFilepath.$newFilename);
        }

        return $this->render('buyer/import.html.twig',
            array('form' => $form->createView(), 'importCount' => $importCount, 'errors' => $errors)
        );

    }

    /**
     * Lists all Buyer entities.
     *
     * @Route("/", name="buyer_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $buyers = $em->getRepository('AppBundle:Buyer')->findAll();

        return $this->render('buyer/index.html.twig', array(
            'buyers' => $buyers,
        ));
    }

    /**
     * Lists all Buyer entities.
     *
     * @Route("/export", name="buyer_export")
     * @Method("GET")
     */
    public function exportAction()
    {
        $response = new StreamedResponse();
        $response->setCallback(function(){
            $em = $this->getDoctrine()->getManager();

            $handle = fopen('php://output', 'w+');

            // Add the header of the CSV file
            fputcsv($handle, array('Id','FirstName', 'Surname', 'Email', 'Phone', 'EmailConsent'),',');
            $buyers = $em->getRepository('AppBundle:Buyer')->findAll();
            foreach ($buyers AS $buyer)
            {
                fputcsv(
                    $handle, // The file pointer
                    [
                        $buyer->getId(),
                        $buyer->getFirstName(),
                        $buyer->getLastName(),
                        $buyer->getEmailAddress(),
                        $buyer->getPhone(),
                        $buyer->hasEmailConsent(),
                    ],
                    ',' // The delimiter
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition','attachment; filename="buyer_export_'.date('Ymdhis').'.csv"');

        return $response;
    }

    /**
     * Creates a new Buyer entity.
     *
     * @Route("/new", name="buyer_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $buyer = new Buyer();
        $form = $this->createForm('AppBundle\Form\BuyerType', $buyer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $buyerService = $this->get('buyer.service');

            if ($buyer = $buyerService->addBuyer($buyer)) {
                return $this->redirectToRoute('buyer_show', array('id' => $buyer->getId()));
            }

        }

        return $this->render('buyer/new.html.twig', array(
            'buyer' => $buyer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Buyer entity.
     *
     * @Route("/{id}.{_format}",
     *  defaults = {"_format"="html"},
     *  requirements = { "_format" = "json"},
     *  name="buyer_show"
     * )
     * @Method("GET")
     */
    public function showAction(Buyer $buyer, $_format)
    {
        if ($_format == 'json') {
            return new Response($this->serializer->serialize($buyer, 'json'));
        }

        // otherwise redirect to edit buyer
        return $this->redirectToRoute('buyer_edit',['id' => $buyer->getId()]);
    }

    /**
     * Displays a form to edit an existing Buyer entity.
     *
     * @Route("/{id}/edit", name="buyer_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Buyer $buyer)
    {
        $deleteForm = $this->createDeleteForm($buyer);
        $editForm = $this->createForm('AppBundle\Form\BuyerType', $buyer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buyer);
            $em->flush();

            return $this->redirectToRoute('buyer_edit', array('id' => $buyer->getId()));
        }

        return $this->render('buyer/edit.html.twig', array(
            'buyer' => $buyer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Buyer entity.
     *
     * @Route("/{id}", name="buyer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Buyer $buyer)
    {
        $form = $this->createDeleteForm($buyer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($buyer);
            $em->flush();
        }

        return $this->redirectToRoute('buyer_index');
    }

    /**
     * Creates a form to delete a Buyer entity.
     *
     * @param Buyer $buyer The Buyer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Buyer $buyer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('buyer_delete', array('id' => $buyer->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
