<?php
namespace AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailchimpService {
    protected $apiUrl;
    protected $username;
    protected $apiKey;
    private $container;
    protected static $apiRequestTimeout = 5; // seconds

    /**
     * Setup mailchimp params - must be called after setContainer()
     */
    public function setup() {
        $this->username = $this->container->getParameter('mailchimp_user');
        $this->apiKey = $this->container->getParameter('mailchimp_api_key');
        $this->apiUrl = $this->container->getParameter('mailchimp_api_url');
    }

    /**
     * Injected via services.yml
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->setup();
    }

    /**
     * @param $url
     * @param string $type
     * @param array $data
     * @return mixed
     */
    public function callApi($url, $type='GET', $data=[]) {
        // create curl resource
        $ch = curl_init();

        // authenticate
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->apiKey);


        if ($type == 'GET') {
            // build query string
            $queryStr = http_build_query($data);
        }

        // set url
        $url = $this->apiUrl.$url;
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($type == 'POST') {
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "content-type: application/json",
        ));

        // set data if any
        if ($type == 'POST' && count($data)) curl_setopt($ch,CURLOPT_POSTFIELDS,
            json_encode($data) // mailchimp api is JSON only
        );

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // set timeout
        curl_setopt($ch, CURLOPT_TIMEOUT, self::$apiRequestTimeout);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    /**
     * @param $email
     * @param $firstName
     * @param string $lastName
     * @param null $listId
     */
    public function addSubscriber($email, $firstName, $lastName='', $listId=null) {
        if (!$listId) {
            $listId = $this->container->getParameter('mailchimp_default_list_id');
        }

        $url = '/lists/'.$listId.'/members';

        $data = [
            'email_address' => $email,
            'status' => 'subscribed',
            'merge_fields' => [
                'FNAME' => $firstName,
                'LNAME' => $lastName,
            ]
        ];

        // execute API call
        $response = $this->callApi($url, 'POST', $data);

        // decode JSON response
        $response = json_decode($response);

        // return response if id set
        if (isset($response->id)) return $response;
        else return false; // no id set, call must've failed
    }
}