<?php
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Buyer;

class BuyerService {
    protected $request;
    private $container;
    protected $em;

    /**
     * @param EntityManager $em - injected via services.yml
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Inject request object into controller - configured in services.yml
     * @param RequestStack $request_stack
     */
    public function setRequest(RequestStack $request_stack)
    {
        $this->request = $request_stack->getCurrentRequest();
    }

    /**
     * Injected via services.yml
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Add buyer
     * @param Buyer $buyer
     * @return Buyer|bool
     */
    public function addBuyer(Buyer $buyer) {
        // check if buyer already exists for email address
        $repository = $this->em->getRepository('AppBundle:Buyer');
        $duplicates = $repository->findByEmailAddress($buyer->getEmailAddress());
        if (count($duplicates)) {
            // duplicate found on email address
            $duplicate = $duplicates[0];

            // if email and firstname do not match, then we have an unmatched duplicate based on email address
            if ($duplicate->getEmailAddress() == $buyer->getEmailAddress() &&
                ($duplicate->getFirstName() == $buyer->getFirstName() || $duplicate->getLastName() == $buyer->getLastName())) {
                // duplicate is a match, simply return a successful result
                // update email consent if set
                if ($buyer->getEmailConsent() == true && ($duplicate->getEmailConsent() == false)) {
                    // update existing buyer email consent
                    $duplicate->setEmailConsent(true);
                    $this->em->persist($duplicate);
                    $this->em->flush();
                    return $duplicate;
                }
                else {
                    // straight duplicate, just return it
                    return $duplicate;
                }
            }
            else {
                return false;
            }
        }
        else {
            // add to Mailchimp
            $mc = $this->container->get('mailchimp.service');
            $result = $mc->addSubscriber($buyer->getEmailAddress(), $buyer->getFirstName(), $buyer->getLastName());

            // otherwise we're adding a new buyer!
            $this->em->persist($buyer);
            $this->em->flush();
        }
        return $buyer;
    }
}