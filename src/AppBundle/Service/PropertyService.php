<?php
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Property;

class PropertyService {
    protected $request;

    protected $em;

    /**
     * @param EntityManager $em - injected via services.yml
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }



    /**
     * Inject request object into controller - configured in services.yml
     * @param RequestStack $request_stack
     */
    public function setRequest(RequestStack $request_stack)
    {
        $this->request = $request_stack->getCurrentRequest();
    }

    /**
     * @return array of unique primary & secondary locations
     */
    public function getLocations() {
        $locations = [];

        $repository = $this->em
            ->getRepository('AppBundle:Property');
        $qb = $repository->createQueryBuilder('p');

        // get primary locations
        $qb->select('p.primaryLocation')
            ->where('p.status IN(:statuses)')
            ->groupBy('p.primaryLocation')
            ->setParameter('statuses', [
                Property::STATUS_ACTIVE,
                Property::STATUS_UNDER_CONTRACT
            ]);;

        $query = $qb->getQuery();
        $data = $query->getResult();

        foreach ($data AS $arr) {
            if ($arr['primaryLocation'] != '') $locations[]= $arr['primaryLocation'];
        }

        // get secondary locations
        $qb->select('p.secondaryLocation')
            ->where('p.status IN(:statuses)')
            ->groupBy('p.secondaryLocation')
            ->setParameter('statuses', [
                Property::STATUS_ACTIVE,
                Property::STATUS_UNDER_CONTRACT
            ]);

        $query = $qb->getQuery();
        $data = $query->getResult();

        foreach ($data AS $arr) {
            if ($arr['secondaryLocation'] != '') $locations[]= $arr['secondaryLocation'];
        }
        $locations = array_unique($locations);
        sort($locations);

        return $locations;
    }

    /**
     * @return array of properties as per request filters
     */
    public function getProperties() {
        $request = $this->request;
        $featured = $request->query->get('featured');
        $status = $request->query->get('status');
        $location = $request->query->get('location');
        $minBeds = $request->query->get('min-beds');
        $minIncome = $request->query->get('min-income');
        $minPrice = $request->query->get('min-price');
        $maxPrice = $request->query->get('max-price');

        $repository = $this->em
            ->getRepository('AppBundle:Property');
        $qb = $repository->createQueryBuilder('p');

        $qb->select('p')
            ->orderBy('p.createdAt','DESC');

        // only include featured listings
        if ($featured) {
            $qb->andWhere('p.featured = :featured');
            $qb->setParameter('featured', true);
        }

        // filter by status
        if ($status) {
            // handle custom filters
            if ($status == 'For Sale') {
                $qb->andWhere('p.status IN(:statuses)');
                $qb->setParameter('statuses', [
                    Property::STATUS_ACTIVE,
                    Property::STATUS_UNDER_CONTRACT
                ]);
            }
            else {
                // standard pass through filter
                $qb->andWhere('p.status = :status');
                $qb->setParameter('status', $status);
            }
        }

        // filter by location
        if ($location) {
            $qb->andWhere('p.primaryLocation = :location OR p.secondaryLocation = :location');
            $qb->setParameter('location', $location);
        }

        // filter by min beds
        if ($minBeds) {
            $qb->andWhere('p.managerUnitSize >= :minBeds');
            $qb->setParameter('minBeds', $minBeds);
        }

        // filter by min beds
        if ($minIncome) {
            $qb->andWhere('p.netProfit >= :minIncome');
            $qb->setParameter('minIncome', $minIncome);
        }

        // filter by min price
        if ($minPrice) {
            $qb->andWhere('p.totalPrice >= :minPrice');
            $qb->setParameter('minPrice', $minPrice);
        }

        // filter by max price
        if ($maxPrice) {
            $qb->andWhere('p.totalPrice <= :maxPrice');
            $qb->setParameter('maxPrice', $maxPrice);
        }

        $query = $qb->getQuery();
        $data = $query->getResult();

        return $data;
    }
}